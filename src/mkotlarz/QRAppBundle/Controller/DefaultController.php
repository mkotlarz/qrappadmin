<?php

namespace mkotlarz\QRAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    
    private $timetables;
    private $teachers;
    private $rooms;
    
    
    public function indexAction()
    {
		$user = $this->getUser();
    }
    
    /**
     * Wyświetla plan zajęc dla danej klasy {@link mkotlarz\QRAppBundle\Entity\Timetable} nie zalogowanym userom
     */
    public function timetablesAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $request = $this->getRequest();
        if(!empty($_GET['timetable_id']))
            $timetable = $repository->find($_GET['timetable_id']);
        else
            $timetable = $repository->findFirst();
        $this->InitializeMembers();
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsOrderedByHours($timetable->getId());
        $replacements_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Replacement');
        $replacements = $replacements_repository->getReplacementsForTimetable($timetable->getId());
        $rooms_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $teachers_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $replacementRooms = array();
        $replacementTeachers = array();
        $i = 0;
        foreach($replacements as $replacement) {
            $replacementRooms[$i] = $rooms_repository->find($replacement->getReplacementRoomId());
            $replacementTeachers[$i] = $teachers_repository->find($replacement->getReplacementTeacherId());
            $i++;
        }
        $checkins_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $checkins_repository->getCheckinsTodayForTimetable($timetable->getId());
        return $this->render('mkotlarzQRAppBundle:Default:show.html.twig', array('name' => $timetable->getName(),
                                                                                 'id' => $timetable->getId(),
                                                                                 'lessons' => $lessons,
                                                                                 'replacements' => $replacements,
                                                                                 'replacements_rooms' => $replacementRooms,
                                                                                 'replacements_teachers' => $replacementTeachers,
                                                                                 'layout_name' =>  'mkotlarzQRAppBundle:Default:timetable.html.twig',
                                                                                 'timetables' => $this->timetables,
                                                                                 'teachers' => $this->teachers,
                                                                                 'rooms' => $this->rooms,
                                                                                 'checkins' => $checkins)); 
    }
    
    /**
     * Wyświetla plan zajęc dla danego nauczyciela {@link mkotlarz\QRAppBundle\Entity\Teacher} nie zalogowanym userom
     */
    public function teachersAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $request = $this->getRequest();
        if(!empty($_GET['teacher_id']))
            $teacher = $repository->find($_GET['teacher_id']);
        else
            $teacher = $repository->findFirst();
        $this->InitializeMembers();
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereTeacherIdOrderedByHours($teacher->getId());
        $replacements_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Replacement');
        $replacements = $replacements_repository->getReplacementsForTeacher($teacher->getId());
        $rooms_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $teachers_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $replacementRooms = array();
        $replacementTeachers = array();
        $i = 0;
        foreach($replacements as $replacement) {
            $replacementRooms[$i] = $rooms_repository->find($replacement->getReplacementRoomId());
            $replacementTeachers[$i] = $teachers_repository->find($replacement->getReplacementTeacherId());
            $i++;
        }
        $checkins_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $checkins_repository->getCheckinsTodayForTeacher($teacher->getId());
        return $this->render('mkotlarzQRAppBundle:Default:show.html.twig', array('name' => (string)$teacher,
                                                                                 'id' => $teacher->getId(),
                                                                                 'lessons' => $lessons,
                                                                                 'replacements' => $replacements,
                                                                                 'replacements_rooms' => $replacementRooms,
                                                                                 'replacements_teachers' => $replacementTeachers,
                                                                                 'layout_name' =>  'mkotlarzQRAppBundle:Default:teacher.html.twig',
                                                                                 'timetables' => $this->timetables,
                                                                                 'teachers' => $this->teachers,
                                                                                 'rooms' => $this->rooms,
                                                                                 'checkins' => $checkins)); 
    }
    
    /**
     * Wyświetla plan zajęc dla danego pomieszczenia {@link mkotlarz\QRAppBundle\Entity\Room} nie zalogowanym userom
     */
    public function roomsAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $request = $this->getRequest();
        if(!empty($_GET['room_id']))
            $room = $repository->find($_GET['room_id']);
        else
            $room = $repository->findFirst();
        $this->InitializeMembers();
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereRoomIdOrderedByHours($room->getId());
        $replacements_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Replacement');
        $replacements = $replacements_repository->getReplacementsForRoom($room->getId());
        $rooms_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $teachers_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $replacementRooms = array();
        $replacementTeachers = array();
        $i = 0;
        foreach($replacements as $replacement) {
            $replacementRooms[$i] = $rooms_repository->find($replacement->getReplacementRoomId());
            $replacementTeachers[$i] = $teachers_repository->find($replacement->getReplacementTeacherId());
            $i++;
        }
        $checkins_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $checkins_repository->getCheckinsTodayForRoom($room->getId());
        return $this->render('mkotlarzQRAppBundle:Default:show.html.twig', array('name' => $room->getNumber(),
                                                                                 'id' => $room->getId(),
                                                                                 'lessons' => $lessons,
                                                                                 'replacements' => $replacements,
                                                                                 'replacements_rooms' => $replacementRooms,
                                                                                 'replacements_teachers' => $replacementTeachers,
                                                                                 'layout_name' =>  'mkotlarzQRAppBundle:Default:room.html.twig',
                                                                                 'timetables' => $this->timetables,
                                                                                 'teachers' => $this->teachers,
                                                                                 'rooms' => $this->rooms,
                                                                                 'checkins' => $checkins)); 
    }
    
    public function emailLayoutTestAction() {
        return $this->render('mkotlarzQRAppBundle:Email:activationEmail.html.twig', array());
    }
    
    private function getAllFromRepository($repositoryName) {
        $repository = $this->getDoctrine()->getRepository($repositoryName);
        return $repository->findAll();
        
    }
    
    private function InitializeMembers() {
        $this->timetables = $this->getAllFromRepository('mkotlarzQRAppBundle:Timetable');
        $this->teachers = $this->getAllFromRepository('mkotlarzQRAppBundle:Teacher');
        $this->rooms = $this->getAllFromRepository('mkotlarzQRAppBundle:Room');
    }
}
