<?php

namespace mkotlarz\QRAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use mkotlarz\QRAppBundle\Entity\Teacher;
use mkotlarz\QRAppBundle\Entity\Checkin;

class JSONGeneratorController extends Controller
{
    /**
     * Generuje JSON z lekcjami dla {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number $school_id    
     * @param   Number $timetable_id 
     */
    public function generateTimetableJSONAction($school_id, $timetable_id)
    {   
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetable = $repository->find($timetable_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsOrderedByHours($timetable->getId());
		$checkinsRepository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
		$checkins = $checkinsRepository->getCheckinsTodayForTimetable($timetable->getId());
        $json = $this->generateJson($lessons, $checkins);
		echo $json;
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /**
     * Generuje JSON z lekcjami dla {@link mkotlarz\QRAppBundle\Entity\Teacher}
     * @param   Number $school_id  
     * @param   Number $teacher_id 
     */
    public function generateTeacherJSONAction($school_id, $teacher_id)
    {   
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->find($teacher_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereTeacherIdOrderedByHours($teacher->getId());
		$checkinsRepository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
		$checkins = $checkinsRepository->getCheckinsTodayForTeacher($teacher->getId());
        $json = $this->generateJson($lessons, $checkins);
        echo $json;
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /**
     * Generuje JSON z lekcjami dla {@link mkotlarz\QRAppBundle\Entity\Room}
     * @param   Number $school_id 
     * @param   Number $room_id   
     */
    public function generateRoomJSONAction($school_id, $room_id)
    {   
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $room = $repository->find($room_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereRoomIdOrderedByHours($room->getId());
		$checkinsRepository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
		$checkins = $checkinsRepository->getCheckinsTodayForRoom($room->getId());
        $json = $this->generateJson($lessons, $checkins);
        echo $json;
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
        
    /**
     * Enumeruje plany zajęc {@link mkotlarz\QRAppBundle\Entity\Timetable} do JSON
     */
    public function enumerateTimetablesJSONAction() {
        $timetables = $this->getAllFromRepository('mkotlarzQRAppBundle:Timetable');
        $json = '{ "data": {"timetables": [';
        $i = 0;
        foreach ($timetables as $timetable) {
            if($i !=0)
                $json .=", ";
            $json .= '{';
            $json .= '"timetable_id": "' . $timetable->getId() . '",
                      "timetable": "' . $timetable->getName() . '",
                      "extra": "' . $timetable->getProfile() . '"
                      }';
            $i++;
        }
        $json .= "]}}";
        echo $json;
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /**
     * Enumeruje plany zajęc {@link mkotlarz\QRAppBundle\Entity\Teacher} do JSON
     *  String JSON
     */
    public function enumerateTeachersJSONAction() {
        $teachers = $this->getAllFromRepository('mkotlarzQRAppBundle:Teacher');
        $json = '{ "data": { "teachers": [';
        $i = 0;
        foreach ($teachers as $teacher) {
            if($i !=0)
                $json .=", ";
            $json .= '{';
            $json .= '"teacher_id": "' . $teacher->getId() . '",
                      "teacher": "' . (string)$teacher . '",
                      "extra": "' . $teacher->getExtra() . '"
                      }';
            $i++;
        }
        $json .= "]}}";
        echo $json;
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
     /**
     * Enumeruje plany zajęc {@link mkotlarz\QRAppBundle\Entity\Room} do JSON
     *  String JSON
     */
    public function enumerateRoomsJSONAction() {
        $rooms = $this->getAllFromRepository('mkotlarzQRAppBundle:Room');
        $json = '{ "data": {"rooms": [';
        $i = 0;
        foreach ($rooms as $room) {
            if($i !=0)
                $json .=", ";
            $json .= '{';
            $json .= '"room_id": "' . $room->getId() . '",
                      "room": "' . $room->getNumber() . '",
                      "extra": "' . $room->getDescription() . '"
                      }';
            $i++;
        }
        $json .= "]}}";
        echo $json;
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
        
    public function teacherCheckinsCallbackAction($room_id, $time, $day, $token, $timetable_id) {
        $json = '{"data": {"message": "';
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->findByToken($token);
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetable = $repository->find($timetable_id);
        if(!empty($teacher[0]) && $teacher[0] != null) {
            $lesson_rep = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
            $lesson = $lesson_rep->getLessonsByRoomIdTimeTeacherId($room_id, $time, $day,  $teacher[0]->getId());
            if(!empty($lesson)) {
                $checkin = new Checkin();
                $checkin->setTimetable($timetable);
                $checkin->setTeacher($lesson[0]->getTeacher());
                $checkin->setRoom($lesson[0]->getRoom());
                $checkin->setHour($lesson[0]->getHour());
                $checkin->setLesson($lesson[0]);
                $checkin->setUpdatedAt(new \DateTime());
                $checkin->setCreatedAt(new \DateTime());
                $checkin->setTeacherToken($token);
                $em = $this->getDoctrine()->getManager();
                $em->persist($checkin);
                $em->flush();
                $json .= "Checkin saved successfully";
            } else
                $json .= "Lesson is empty";
        } else
            $json .= "Teacher token not verified!";
        $json .= '" }}';
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json, 'action' => 'rooms')); 
    }
    
    public function teacherCheckinsAction($school_id, $teacher_token) {
        $json = '{"data": { "checkins": [';
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->findByToken($teacher_token);
        if($teacher != null )
            if($teacher[0] == null)
                return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => "Invalid token", 'action' => 'teacher')); 
            else
                $teacher = $teacher[0];
        else
            return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => "Invalid token", 'action' => 'teacher')); 
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $repository->getTeacherCheckins($teacher->getId());
        $bIsFirst = true;
        foreach($checkins as $checkin) {
            if(!$bIsFirst)
                $json .= ", ";
            else
                $bIsFirst = false;
            $json .= "{";
            $json .= '"timetable_id": "' . $checkin->getTimetable()->getId() . '", ';
            $json .= '"timetable": "' . $checkin->getTimetable() . '", ';
            $json .= '"room_id": "' . $checkin->getRoom()->getId() . '", ';
            $json .= '"room": "' . $checkin->getRoom() . '", ';
            $json .= '"subject": "' . $checkin->getLesson()->getSubject()->getName() . '", ';
            $json .= '"hour": "' . $checkin->getLesson()->getHour() . '", ';
            $json .= '"day": "' . $checkin->getLesson()->getDay() . '"';
            $json .= "}";
        }
        $json .= ']}}';
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json, 'action' => 'rooms')); 
    }
    
    public function enumerateReplacementsJSONAction() {
        $replacements = $this->getAllFromRepository('mkotlarzQRAppBundle:Replacement');
        $json = '{"data": { "replacements": [';
        $isFirst = true;
        foreach($replacements as $replacement) {
            if(!$isFirst)
                $json = ' ,';
            else 
                $isFirst = false;
            $json .= '{';
            $json .= '"for": "' . $replacement->getLesson()->getTimetable() . '", ';
            $json .= '"with": "' . $replacement->getLesson()->getTeacher() . '", ';
            $json .= '"in": "' . $replacement->getLesson()->getRoom() . '", ';
            $json .= '"replacement_with": "' . $this->getSimpleRecordById('mkotlarzQRAppBundle:Teacher', $replacement->getReplacementTeacherId()) . '", ';
            $json .= '"replacement_in": "' . $this->getSimpleRecordById('mkotlarzQRAppBundle:Room', $replacement->getReplacementRoomId()) . '", ';
            $json .= '"hour": "' . $replacement->getLesson()->getHour() . '", ';
            $json .= '"subject": "' . $replacement->getLesson()->getSubject() . '", ';
            $json .= '"replacement_subject": "' . $this->getSimpleRecordById('mkotlarzQRAppBundle:Subject', $replacement->getReplacementSubjectId()) . '", ';
            $json .= '"is_free": "' . $replacement->getIsEmpty() . '", ';
            $json .= '"day": "' . $replacement->getLesson()->getDay() . '"';
            $json .= '}';
        }
        $json .= ' ]}}';
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json, 'action' => 'replacements')); 
    }
    
    /*
     ***********************************************
     
     Private methods
     
     ***********************************************
     */
    
    /**
    *   Generuje JSON z lekcjami oraz zastępstwami
    */
    private function generateJson($lessons, $checkins) {
        $school = $this->getSchool();
		$checkinsRepository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $json =  '{"data": { "school_name": "' . $school->getName() . '",';
        $json .= '"school_token": "' . $school->getToken() . '",';
        $json .= '"hours": [';
        
        if(!empty($lessons[0])) {
            $last_hour = (string)$lessons[0]->getHour();
        }
        $i = 1;
        $j = 1;
        $is_first = true;
        $is_first_hour = true;
        foreach($lessons as $lesson) {
            $current_hour = (string)$lesson->getHour();
            if($last_hour != $current_hour || $j == 1) {
                $last_hour = $current_hour;
                 $i = 1;
                if($j != 1)
                    $json .= ']}';
                if(!$is_first) {
                    $json .= ', ';
                    $is_first = false;
                }
                $json .= '{ "start": "' . $lesson->getHour()->getStart()->format("H:i") . '", ';
                $json .= '"end": "' . $lesson->getHour()->getEnd()->format("H:i") . '", ';
                $json .= '"hour_id": "' . $lesson->getHour()->getId() . '", ';
                $json .= '"updated_at" : "' . $lesson->getHour()->getUpdatedAt() . '", ';
                $json .= '"lessons": [';
                $is_first_lesson = true;
                $is_first = false;
            }
            if($is_first_lesson)
                $is_first_lesson = false;   
            else
                $json .= ', ';
            $subject = $lesson->getTeacher()->getSubject();
            $json .= '{';
            $json .= '"subject": "' . $lesson->getSubject()->getName() . '", ';
            $json .= '"subject_id": "' . $lesson->getSubject()->getId() . '", ';
            $json .= '"subject_updated_at": "' . $lesson->getSubject()->getUpdatedAt() . '", ';
            $json .= '"room": "' . $lesson->getRoom()->getNumber() . '", ';
            $json .= '"room_id": "' . $lesson->getRoom()->getId() . '", ';
            $json .= '"room_teacher_id": "' . $lesson->getRoom()->getTeacher()->getId() . '", ';
            $json .= '"room_extra": "' . $lesson->getRoom()->getDescription() . '", ';
            $json .= '"room_updated_at": "' . $lesson->getRoom()->getUpdatedAt() . '", ';
            $json .= '"teacher": "' . (string)$lesson->getTeacher() . '", ';
            $json .= '"teacher_id": "' . $lesson->getTeacher()->getId() . '", ';
            if($subject != null)
                $json .= '"teacher_subject_id": "' . $subject->getId() . '", ';
            else
                $json .= '"teacher_subject_id": "0", ';
            $json .= '"teacher_name": "' . $lesson->getTeacher()->getName() . '", ';
            $json .= '"teacher_surname": "' . $lesson->getTeacher()->getSurname() . '", ';
            $json .= '"teacher_city": "' . $lesson->getTeacher()->getCity() . '", ';
            $json .= '"teacher_street": "' . $lesson->getTeacher()->getStreet() . '", ';
            $json .= '"teacher_postal": "' . $lesson->getTeacher()->getPostal() . '", ';
            $json .= '"teacher_extra": "", ';
            $json .= '"teacher_updated_at": "' . $lesson->getTeacher()->getUpdatedAt() . '", ';
            $json .= '"timetable": "' . $lesson->getTimetable()->getName() . '", ';
            $json .= '"timetable_id": "' . $lesson->getTimetable()->getId() . '", ';
            $json .= '"timetable_extra": "' . (string)$lesson->getTimetable()->getTeacher() . '", ';
            $json .= '"day": "' . $lesson->getDay() . '", ';
            $json .= '"lesson_id": "' . $lesson->getId() . '", ';
            $json .= '"updated_at": "' . $lesson->getUpdatedAt() . '", ';
            $json .= '"replacement": { ';
            if($lesson->getReplacements()->last() != null) {
                $json .= '"is_replacement_null": "false", ';
                $json .= '"replacement_id": "' . $lesson->getReplacements()->last()->getId() . '", ';
                $json .= '"replacement_teacher_id": "' . $lesson->getReplacements()->last()->getReplacementTeacherId() . '", ';
                $json .= '"replacement_subject_id": "' . $lesson->getReplacements()->last()->getReplacementSubjectId() . '", ';
                $json .= '"replacement_room_id": "' . $lesson->getReplacements()->last()->getReplacementRoomId() . '", ';
                $json .= '"replacement_hour_id": "' . $lesson->getReplacements()->last()->getReplacementHourId() . '", ';
                $json .= '"description": "' . $lesson->getReplacements()->last()->getDescription() . '", ';
                $json .= '"is_free": "' . $lesson->getReplacements()->last()->getIsEmpty() . '", ';
                $json .= '"updated_at": "' . $lesson->getReplacements()->last()->getUpdatedAt() . '"';
            } else
                $json .= '"is_replacement_null": "true"';
            $json .= ' }';
            $json .= '}';
            $j++;
        }
        $json .= "]}], ";
		$json .= '"checkins": [';
		$bIsFirstCheckin = true;
		foreach($checkins as $checkin) {
			if($bIsFirstCheckin == true) {
				$bIsFirstCheckin = false;	
			} else {
				$json .= ",";	
			}
			$json.= "{";
			$json .= '"hour": "' . $checkin->getHour() . '",';
			$json .= '"by": "' . $checkin->getTeacher() . '",';
			$json .= '"room": "' . $checkin->getRoom() . '",';
			$json .= '"timetable": "' . $json->getTimetable() . '"';
			$json .= "}";
		}
		$json .= ']';
		$json .= "}}";
        return $json;
    }
    
    private function getAllFromRepository($repositoryName) {
        $repository = $this->getDoctrine()->getRepository($repositoryName);
        return $repository->findAll(); 
    }
    
    private function getSimpleRecordById($repositoryName, $id) {
        $repository = $this->getDoctrine()->getRepository($repositoryName);
        return $repository->find($id);
    }
    
    private function getSchool() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:School');
        return $repository->find(1);
    }
    

}
