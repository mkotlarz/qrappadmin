<?php

namespace mkotlarz\QRAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class QRCodeGeneratorController extends Controller
{
    /**
     * Generuje kod QR pozwalający na pobranie danych o {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number   $timetable_id
     *  Object KOD QR
     */
    public function timetableAction($timetable_id)
    {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetable = $repository->find($timetable_id);
        $school = $this->getSchool();
        $message = '{"data": { ';
        $message .= '"url": "/mobile_api/timetable/' . $school->getId() . '/' . $timetable->getId() . '/json"';
        $message .= ', "school_token": "' . $school->getToken() . '"';
        $message .= ', "type": "timetable"';
        $message .= ', "id": "' . $timetable->getId() . '"';
        $message .= ', "extra": "' . (string)$timetable->getTeacher() . '"';
        $message .= '}}';
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:QRCodeGenerator:_base.html.twig', array(
            'message' => $message,
            'action' => 'timetables',
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
        ));    }

    /**
     * Generuje kod QR pozwalający na pobranie danych o {@link mkotlarz\QRAppBundle\Entity\Teacher}
     * @param   Number   $teacher_id
     *  Object Kod QR
     */
    public function teacherAction($teacher_id)
    {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->find($teacher_id);
        $school = $this->getSchool();
        $message = '{"data": { ';
        $message .= '"url": "/mobile_api/teacher/' . $school->getId() . '/' . $teacher->getId() . '/json"';
        $message .= ', "school_token": "' . $school->getToken() . '"';
        $message .= ', "type": "teacher"';
        $message .= ', "id": "' . $teacher->getId() . '"';
        $message .= ', "extra": "' . $room->getDescription() . '"';
        $message .= '}}';
        echo $  
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:QRCodeGenerator:_base.html.twig', array(
            'message' => $message,
            'action' => 'teachers',
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
        ));    }

    /**
     * Generuje kod QR pozwalający na pobranie danych o {@link mkotlarz\QRAppBundle\Entity\Room}
     * @param   Number   $room_id
     *  Object Kod QR
     */
    public function roomAction($room_id)
    {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $room = $repository->find($room_id);
        $school = $this->getSchool();
        $message = '{"data": { ';
        $message .= '"url": "/mobile_api/room/' . $school->getId() . '/' . $room->getId() . '/json"';
        $message .= ', "school_token": "' . $school->getToken() . '"';
        $message .= ', "type": "room"';
        $message .= ', "id": "' . $room->getId() . '"';
        $message .= ', "extra": ""';
        $message .= '}}';
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:QRCodeGenerator:_base.html.twig', array(
            'message' => $message, 
            'action' => 'rooms',
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements']));    
    }
    
    public function roomAllAction()
    {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $rooms = $repository->findAll();
        $school = $this->getSchool();
        $messages = array();
        $i = 0;
        foreach($rooms as $room) {
            $array = array();
            $message = '{"data": { ';
            $message .= '"url": "/mobile_api/room/' . $school->getId() . '/' . $room->getId() . '/json"';
            $message .= ', "school_token": "' . $school->getToken() . '"';
            $message .= ', "type": "room"';
            $message .= ', "id": "' . $room->getId() . '"';
            $message .= ', "extra": "' . $room->getDescription() . '"';
            $message .= '}}';  
            $array["message"] = $message;
            $array["name"] = $room->getNumber();
            $messages[$i] = $array;
            $i++;
        }
        return $this->render('mkotlarzQRAppBundle:QRCodeGenerator:_all.html.twig', array(
            'messages' => $messages
        ));    
    }
    
    public function teacherCheckinsAction($room_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $room = $repository->find($room_id);
        $school = $this->getSchool();
        $message = '{"data": { ';
        $message .= '"url": "/mobile_api/checkins/' . $room->getId() . '/{TIME}/{DAY}/{TEACHER_TOKEN}/{TIMETABLE_ID}/callback"';
        $message .= ', "school_token": "' . $school->getToken() . '"';
        $message .= ', "type": "room"';
        $message .= ', "id": "' . $room->getId() . '"';
        $message .= ', "extra": "' . $room->getDescription() . '"';
        $message .= '}}';
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:QRCodeGenerator:_base.html.twig', array(
            'message' => $message, 'action' => 'rooms',
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
        )); 
    }
    
    public function teacherCheckinsAllAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $rooms = $repository->findAll();
        $school = $this->getSchool();
        $messages = array();
        $i = 0;
        foreach($rooms as $room) {
            $message = '{"data": { ';
            $message .= '"url": "/mobile_api/checkins/' . $room->getId() . '/{TIME}/{DAY}/{TEACHER_TOKEN}/{TIMETABLE_ID}/callback"';
            $message .= ', "school_token": "' . $school->getToken() . '"';
            $message .= ', "type": "room"';
            $message .= ', "id": "' . $room->getId() . '"';
            $message .= ', "extra": "' . $room->getDescription() . '"';
            $message .= '}}';
            $array = array();
            $array["message"] = $message;
            $array["name"] = $room->getNumber() . " - Autoryzacja";
            $messages[$i] = $array;
            $i++;
        }
        return $this->render('mkotlarzQRAppBundle:QRCodeGenerator:_all.html.twig', array(
            'messages' => $messages
        )); 
    }
    
    private function getRecordsCount($table) {
        $repository = $this->getDoctrine()->getRepository($table);
        $result = $repository->getCount();
        return $result;
    }
    
    private function getRecordsCountFromAllTables() {
        $user = $this->getUser();
        $data = array();
        $data['users'] = $this->getRecordsCount('mkotlarzQRAppBundle:User');
        $data['rooms'] = $this->getRecordsCount('mkotlarzQRAppBundle:Room');
        $data['hours'] = $this->getRecordsCount('mkotlarzQRAppBundle:Hour');
        $data['subjects'] = $this->getRecordsCount('mkotlarzQRAppBundle:Subject');
        $data['teachers'] = $this->getRecordsCount('mkotlarzQRAppBundle:Teacher');
        $data['timetables'] = $this->getRecordsCount('mkotlarzQRAppBundle:Timetable');
        $data['replacements'] = $this->getRecordsCount('mkotlarzQRAppBundle:Replacement');
        $data['user_name'] = $user->getName() . ' ' . $user->getSurname();
        $data['user'] = $user;
        return $data;
    }
    
    private function getSchool() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:School');
        $school = $repository->find(1);
        return $school;
    }
}
