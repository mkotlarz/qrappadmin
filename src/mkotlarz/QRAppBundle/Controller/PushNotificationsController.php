<?php

namespace mkotlarz\QRAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use mkotlarz\QRAppBundle\Entity\PushDevices;
use RMS\PushNotificationsBundle\Message\AndroidMessage;

class PushNotificationsController extends Controller
{
    public function addDeviceInstanceAction(Request $request)
    {
        $device_id = $request->request->get("device_id");
        $device_name = $request->request->get("device_name");
        $device_country = $request->request->get("device_country");

        $em = $this->getDoctrine()->getManager();
        $device = $em->getRepository("mkotlarzQRAppBundle:PushDevices")->findByDeviceId($device_id);
        if(!$device) {
            // Create new device
            $device = new PushDevices();
            $device->setDeviceId($device_id);
            $device->setDeviceName($device_name);
            $device->setCountry($device_country);

            // Save device
            $em->persist($device);
            $em->flush();
        }
        return $this->render('mkotlarzQRAppBundle:PushNotifications:addDeviceInstance.html.twig', array(
            ));    }

    public function sendPushMessageAction(Request $request)
    {
        $message = $request->query->get("message");
        $returnToRouteName = $request->query->get("url");

        $em = $this->getDoctrine()->getManager();
        $devices = $em->getRepository("mkotlarzQRAppBundle:PushDevices")->findAll();
        foreach($devices as $device) {
            $pushMessage = new AndroidMessage();
            $pushMessage->setGCM(true);
            $pushMessage->setMessage($message);
            $pushMessage->setDeviceIdentifier($device-> getDeviceId());
            $this->container->get('rms_push_notifications')->send($pushMessage);
        }

        return $this->redirect($returnToRouteName);
    }

}
