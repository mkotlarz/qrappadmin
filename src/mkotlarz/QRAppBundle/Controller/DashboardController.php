<?php

namespace mkotlarz\QRAppBundle\Controller;

/**
 * Główny kontroler dla dashboardu.
 * <p>
 * 	Pozwala na tworzenie zarządzanie planem zajęć
 * </p>  
 */

use mkotlarz\QRAppBundle\Entity\School;
use mkotlarz\QRAppBundle\Form\Type\SchoolType;
use mkotlarz\QRAppBundle\Entity\Room;
use mkotlarz\QRAppBundle\Form\Type\RoomType;
use mkotlarz\QRAppBundle\Entity\Hour;
use mkotlarz\QRAppBundle\Form\Type\HourType;
use mkotlarz\QRAppBundle\Entity\Subject;
use mkotlarz\QRAppBundle\Form\Type\SubjectType;
use mkotlarz\QRAppBundle\Entity\Teacher;
use mkotlarz\QRAppBundle\Form\Type\TeacherType;
use mkotlarz\QRAppBundle\Entity\Timetable;
use mkotlarz\QRAppBundle\Form\Type\TimetableType;
use mkotlarz\QRAppBundle\Entity\Lesson;
use mkotlarz\QRAppBundle\Form\Type\LessonType;
use mkotlarz\QRAppBundle\Form\Type\LessonRoomType;
use mkotlarz\QRAppBundle\Form\Type\LessonTeacherType;
use mkotlarz\QRAppBundle\Entity\Replacement;
use mkotlarz\QRAppBundle\Form\Type\ReplacementType;
use mkotlarz\QRAppBundle\Entity\User;
use mkotlarz\QRAppBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \DateTime;;

class DashboardController extends Controller
{
    /**
     * Wyświetla stronę główną dashboradu
     *  [Domyślny layout: mkotlarzQRAppBundle:Dashboard:index.html.twig]
     */
    public function indexAction()
    {  
        $replacements = $this->getAllFrom('mkotlarzQRAppBundle:Replacement');
        $timetables = $this->getAllFrom('mkotlarzQRAppBundle:Timetable');
        $data = $this->getRecordsCountFromAllTables();
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teachers = $repository->findLastAddedTeachers(4);
        $hour = strftime("%H");
        $minutes = strftime("%M");
        $date = $hour . ":" . $minutes;
        
        return $this->render('mkotlarzQRAppBundle:Dashboard:index.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'replacements' => $replacements,
            'timetables' => $timetables,
            'teachers' => $teachers,
            'time' => $date
        ));    
    }
    
    public function usersAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:User');
        $users = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:users.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'users' => $users
        )); 
    }
    
    public function usersAddAction(Request $request) {
        $user = new User();
        
        $form = $this->createForm(new UserType(), $user);
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $formData = $form->getData();
                $password = $this->randomPassword();
                $formData->setPlainPassword($password);
                $formData->setSuperAdmin(true) ;
                $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                $formData->setConfirmationToken($tokenGenerator->generateToken());
                echo $password;
                $formData = $this->setCreateData($formData);
                $this->saveEntity($formData);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('dashboard/users');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'user',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj użytkownika',
            'top_menu_content' => '> <a href="/dashboard/users">Użytkownicy</a> > <a href="/dashboard/user/add">Dodaj użytkownika</a>'
        )); 
    }
    
    public function userRemoveAction(Request $request, $user_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:User');
        $user = $repository->find($user_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return $this->redirect('/dashboard');
    }
    
    public function userSendActivationEmailAction($user_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:User');
        $user = $repository->find($user_id);
        $mailer = $this->get('mailer');
        $message = $mailer->createMessage()
            ->setSubject('Activation email')
            ->setFrom('biuro@mkotlarz.pl')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                'mkotlarzQRAppBundle:Email:activationEmail.html.twig',
                array('link' => 'http://localhost/register/confirm/' . $user->getConfirmationToken(),
                       'login' => $user->getEmail(),
                       'password' => 'Hasło zostało wygenerowane automatycznie, prosimy o jego zmiane za pomocą adresu podanego poniżej'
                     )
                )
            );
        $message->setContentType("text/html");
        $mailer->send($message);
        return $this->redirect('/dashboard/users');
    }
    
    /**
     * Listuje wszystkie szkoły utworzone przez danego usera  {@link mkotlarz\QRAppBundle\Entity\School}
     */
    public function schoolsAction() {
        $user = $this->getUser();
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:School');
        $schools = $repository->findAllBelongsToUser($user->getId());
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:schools.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'schools' => $schools
        )); 
    }
    
    /**
     * Pozwala na dodanie szkoły
     * @param   {@link Symfony\Component\HttpFoundation\Request} $request [Adres url, zawiera ID usera]  @link mkotlarz\QRAppBundle\Entity\School
     */
    public function schoolsAddAction(Request $request) {
        
        $school = new School();
        
        $form = $this->createForm(new SchoolType(), $school);
        $data = $this->getRecordsCountFromAllTables();
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData->setToken($this->generateToken());
                $formData->setIsActive(true);
                $formData->setIsExpired(false);
                $formData = $this->setCreateData($formData);
                $this->saveEntity($formData);
            }
        }
        $back_url = $this->generateBackUrl('dashboard/schools');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'schools',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements']
        )); 
    }
    
    /**
     * Listuje wszystkie pomieszczenia:  {@link mkotlarz\QRAppBundle\Entity\Room}
     */
    public function roomsAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $rooms = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:rooms.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'rooms' => $rooms
        )); 
    }
    
    /**
     * Pozwala na dodanie pomieszczenia do bazy danych(dla danej szkoły)  {@link mkotlarz\QRAppBundle\Entity\Room}
     */
    public function roomsAddAction(Request $request) {
        
        $room = new Room();
        
        $form = $this->createForm(new RoomType(), $room);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setSchool($this->getSchool());
                $data = $this->setCreateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('rooms');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'rooms',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj pokój',
            'top_menu_content' => '> <a href="/dashboard/rooms">Pokoje</a> > <a href="/dashboard/room/add">Dodaj pokój</a>'
        )); 
    }
    
    public function roomRemoveAction(Request $request, $room_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $room = $repository->find($room_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($room);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    /**
     * Listuje wszystkie godziny zajęć:  {@link mkotlarz\QRAppBundle\Entity\Hour}
     */
    public function hoursAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Hour');
        $hours = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:hours.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'hours' => $hours
        )); 
    }
    
    /**
     * Pozwala na dodanie godziny do planu zajęć  {@link mkotlarz\QRAppBundle\Entity\Hour}
     */
    public function hoursAddAction(Request $request) {
        
        $hour = new Hour();
        
        $form = $this->createForm(new HourType(), $hour);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setSchool($this->getSchool());
                $data = $this->setCreateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('hours');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'hours',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj godzinę',
            'top_menu_content' => '> <a href="/dashboard/hours">Godziny</a> > <a href="/dashboard/hour/add">Dodaj godzinę</a>'
        )); 
    }
    
    public function hourRemoveAction(Request $request, $hour_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Hour');
        $hour = $repository->find($hour_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($hour);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    /**
     * Listuje wszystkie przedmioty  {@link mkotlarz\QRAppBundle\Entity\Subject}
     */
    public function subjectsAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Subject');
        $subjects = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:subjects.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'subjects' => $subjects
        )); 
    }
    
    /**
     * Pozwala na dodanie przedmiotu do bazy danych {@link mkotlarz\QRAppBundle\Entity\Subject}
     * @param   Request  $request
     */
    public function subjectsAddAction(Request $request) {
        
        $subject = new Subject();
        
        $form = $this->createForm(new SubjectType(), $subject);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setCreateData($data);
                $data->setSchool($this->getSchool());
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('subjects');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'subjects',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj przedmiot',
            'top_menu_content' => '> <a href="/dashboard/subjects">Przedmioty</a> > <a href="/dashboard/subject/add">Dodaj przedmiot</a>'
        )); 
    }
    
    public function subjectRemoveAction(Request $request, $subject_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Subject');
        $subject = $repository->find($subject_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($subject);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    /**
     * Listuje wszystkie plany zajęc  w danej szkole {@link mkotlarz\QRAppBundle\Entity\Timetable}
     *  [[Type]] [[Description]]
     */
    public function timetablesAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetables = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:timetables.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'timetables' => $timetables
        )); 
    }
    
    /**
     * Pozwala na dodanie nowego planu zajęc {@link mkotlarz\QRAppBundle\Entity\Subject}
     * @param   Request $request
     */
    public function timetablesAddAction(Request $request) {
        
        $timetable = new Timetable();
        
        $form = $this->createForm(new TimetableType(), $timetable);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setIsActive(true);
                $data = $this->setCreateData($data);
                $data->setSchool($this->getSchool());
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('timetables');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'timetables',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj plan zajęć',
            'top_menu_content' => '> <a href="/dashboard/timetables">Plany zajęć</a> > <a href="/dashboard/timetable/add">Dodaj plan zajęć</a>'
        )); 
    }
    
    public function timetableRemoveAction(Request $request, $timetable_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetable = $repository->find($timetable_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($timetable);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    
    /**
     * Listuje wszystkich nauczycieli w szkole: {@link mkotlarz\QRAppBundle\Entity\Teacher}
     */
    public function teachersAction() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teachers = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:teachers.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'teachers' => $teachers
        )); 
    }
    
    /**
     * Pozwala na dodanie nauczyciela {@link mkotlarz\QRAppBundle\Entity\Teacher}
     * @param   Request $request
     */
    public function teachersAddAction(Request $request) {
        
        $teacher = new Teacher();
        
        $form = $this->createForm(new TeacherType(), $teacher);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setToken($this->generateToken());
                $data->setSchool($this->getSchool());
                $data = $this->setCreateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('teachers');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'teachers',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj nauczyciela',
            'top_menu_content' => '> <a href="/dashboard/teachers">Nauczyciele</a> > <a href="/dashboard/teacher/add">Dodaj nauczyciela</a>'
        )); 
    }
    
    /**
     * Listuje plan zajęc dla danego {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number   $timetable_id ID {@link mkotlarz\QRAppBundle\Entity\Timetable w bazie danych}
     *  Array Lista {@link mkotlarz\QRAppBundle\Entity\Lesson} dla danego planu
     */
    public function timetableAction($timetable_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetable = $repository->find($timetable_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsOrderedByHours($timetable->getId());
        $data = $this->getRecordsCountFromAllTables();
        $checkins_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $checkins_repository->getCheckinsTodayForTimetable($timetable_id);
        return $this->render('mkotlarzQRAppBundle:Dashboard:timetable.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'name' => $timetable->getName(),
            'id' => $timetable->getId(),
            'checkin' => $checkins,
            'lessons' => $lessons
        )); 
    }
    
    public function teacherRemoveAction(Request $request, $teacher_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->find($teacher_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($teacher);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
     /**
     * Listuje plan zajęc dla danego {@link mkotlarz\QRAppBundle\Entity\Teacher}
     * @param   Number   $teacher_id ID {@link mkotlarz\QRAppBundle\Entity\Teacher} w bazie danych
     *  Array Lista {@link mkotlarz\QRAppBundle\Entity\Lesson} dla danego planu
     */
    public function teacherShowAction($teacher_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->find($teacher_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereTeacherIdOrderedByHours($teacher->getId());
        $data = $this->getRecordsCountFromAllTables();
        $checkins_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $checkins_repository->getCheckinsTodayForTeacher($teacher_id);
        return $this->render('mkotlarzQRAppBundle:Dashboard:teacherShow.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'name' => $teacher,
            'id' => $teacher->getId(),
            'checkins' => $checkins,
            'lessons' => $lessons
        )); 
    }
    
    public function teacherSendTokenAction(Request $request, $teacher_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->find($teacher_id);
        $mailer = $this->get('mailer');
        $message = $mailer->createMessage()
            ->setSubject('Teacher token')
            ->setFrom('biuro@mkotlarz.pl')
            ->setTo($teacher->getEmail())
            ->setBody(
                $this->renderView(
                'mkotlarzQRAppBundle:Email:teacherTokenEmail.html.twig',
                array(
                       'token' => $teacher->getToken()
                     )
                )
            )
        ;
        $message->setContentType("text/html");
        $mailer->send($message);
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    /**
     * Pozwala na dodanie {@link mkotlarz\QRAppBundle\Entity\Lesson} do planu {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number $timetable_id ID planu do którego dodajemy
     * @param   Request       $request
     */
    public function teacherLessonAddAction($teacher_id ,Request $request) {
        
        $lesson = new Lesson();
        $teacher = $this->getTeacher($teacher_id);
        $lesson->setTeacher($teacher);
        $form = $this->createForm(new LessonTeacherType(), $lesson);
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setCreateData($data);
                $data->setTeacher($teacher);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('teacher/' . $teacher->getId());
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'teacher/' . $teacher_id . '/lesson',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj lekcje',
            'top_menu_content' => '> <a href="/dashboard/lessons">Lekcje</a> > <a href="/dashboard/room/' . $teacher_id . '/lesson/add">Dodaj lekcje</a>'
        )); 
    }
    
    
    /**
     * Listuje plan zajęc dla danego {@link mkotlarz\QRAppBundle\Entity\Room}
     * @param   Number $room_id ID {@link mkotlarz\QRAppBundle\Entity\Room} w bazie danych
     *  Array  Lista {@link mkotlarz\QRAppBundle\Entity\Lesson} dla danego planu
     */
    public function roomShowAction($room_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $room = $repository->find($room_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereRoomIdOrderedByHours($room->getId());
        $data = $this->getRecordsCountFromAllTables();
        $checkins_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');
        $checkins = $checkins_repository->getCheckinsTodayForRoom($room_id);
        return $this->render('mkotlarzQRAppBundle:Dashboard:roomShow.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'name' => $room->getNumber(),
            'id' => $room->getId(),
            'checkins' => $checkins,
            'lessons' => $lessons
        )); 
    }
    
    /**
     * Pozwala na dodanie {@link mkotlarz\QRAppBundle\Entity\Lesson} do planu {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number $timetable_id ID planu do którego dodajemy
     * @param   Request       $request
     */
    public function roomLessonAddAction($room_id ,Request $request) {
        
        $lesson = new Lesson();
        $room = $this->getRoom($room_id);
        $lesson->setRoom($room);
        $form = $this->createForm(new LessonRoomType(), $lesson);
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setCreateData($data);
                $data->setRoom($room);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('room/' . $room->getId());
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'room/' . $room_id . '/lesson',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj lekcje',
            'top_menu_content' => '> <a href="/dashboard/lessons">Lekcje</a> > <a href="/dashboard/room/' . $room_id . '/lesson/add">Dodaj lekcje</a>'
        )); 
    }
    
    /**
     * Pozwala na dodanie {@link mkotlarz\QRAppBundle\Entity\Lesson} do planu {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number $timetable_id ID planu do którego dodajemy
     * @param   Request       $request
     */
    public function timetableLessonAddAction($timetable_id ,Request $request) {
        
        $lesson = new Lesson();
        $timetable = $this->getTimetable($timetable_id);
        $lesson->setTimetable($timetable);
        $form = $this->createForm(new LessonType(), $lesson);
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setCreateData($data);
                $data->setTimetable($timetable);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('timetable/' . $timetable->getId());
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'timetable/' . $timetable_id . '/lesson',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj lekcje',
            'top_menu_content' => '> <a href="/dashboard/lessons">Lekcje</a> > <a href="/dashboard/timetable/' . $timetable_id . '/lesson/add">Dodaj lekcje</a>'
        )); 
    }
    
    /**
     * Listuje wszystkie {@link mkotlarz\QRAppBundle\Entity\Replacement}
     */
    public function replacementsAllAction() {
        
        $data = $this->getRecordsCountFromAllTables();  
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Replacement');
        $replacements = $repository->findAll();
        $data = $this->getRecordsCountFromAllTables();
        return $this->render('mkotlarzQRAppBundle:Dashboard:replacements.html.twig', array(
            'data' => $data,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'replacements' => $replacements
        )); 
    }
    
    /**
     * Pozwala na dodanie {@link mkotlarz\QRAppBundle\Entity\Replacement}
     * @param   Request       $request
     */
    public function replacementsAddAction(Request $request) {
        $replacement = new Replacement();
        $form = $this->createForm(new ReplacementType($this->getDoctrine()), $replacement);
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setCreateData($data);
                $this->saveEntity($data);
                return $this->redirect($this->generateUrl('send_push_message', array(
                    'url' => $request->get('_route') . $request->get('_route_params'),
                    'message' => 'Nowe zastępstwo zostało dodane'
                    )));
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('replacements');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_add.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'replacements',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj zastępstwo',
            'top_menu_content' => '> <a href="/dashboard/replacements">Zastępstwa</a> > <a href="/dashboard/replacement/add">Dodaj zastępstwo</a>'
        )); 
    }
    
    public function replacementRemoveAction(Request $request, $replacement_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Replacement');
        $replacement = $repository->find($replacement_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($replacement);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    public function lessonRemoveAction(Request $request, $lesson_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lesson = $repository->find($lesson_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($lesson);
        $em->flush();
        $referer = $request->request->get('referer', $request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    public function lessonEditAction(Request $request, $lesson_id) {
        $lesson = $this->getLesson($lesson_id);
        $form = $this->createForm(new LessonType(), $lesson);
        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setUpdateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('timetable/' . $lesson->getTimetable()->getId());
        return $this->render('mkotlarzQRAppBundle:Dashboard:_edit.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'timetable/' . $lesson->getTimetable()->getId() . '/lesson',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Edytuj lekcje',
            'path_request' => 'lesson',
            'id' => $lesson->getId(),
            'top_menu_content' => '> <a href="/dashboard/lessons">Lekcje</a> > <a href="/dashboard/timetable/' . $lesson->getTimetable()->getId() . '/lesson/add">Edytuj lekcje</a>'
        )); 
    }
    
    public function teacherEditAction(Request $request, $teacher_id) {
        $teacher = $this->getTeacher($teacher_id);
        
        $form = $this->createForm(new TeacherType(), $teacher);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setUpdateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('teachers');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_edit.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'teachers',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Edytuj nauczyciela',
            'path_request' => 'teacher',
            'id' => $teacher->getId(),
            'top_menu_content' => '> <a href="/dashboard/teachers">Nauczyciele</a> > <a href="/dashboard/teacher/add">Edytuj nauczyciela</a>'
        )); 
    }
    
    public function roomEditAction(Request $request, $room_id) {
        
        $room = $this->getRoom($room_id);
        
        $form = $this->createForm(new RoomType(), $room);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setUpdateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('rooms');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_edit.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'rooms',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Edytuj pokój',
            'path_request' => 'room',
            'id' => $room->getId(),
            'top_menu_content' => '> <a href="/dashboard/rooms">Pokoje</a> > <a href="/dashboard/room/add">Edytuj pokój</a>'
        )); 
    }
    
    public function timetableEditAction(Request $request, $timetable_id) {
        
        $timetable = $this->getTimetable($timetable_id);
        
        $form = $this->createForm(new TimetableType(), $timetable);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setIsActive(true);
                $data = $this->setUpdateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('timetables');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_edit.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'timetables',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Edytuj plan zajęć',
            'path_request' => 'timetable',
            'id' => $timetable->getId(),
            'top_menu_content' => '> <a href="/dashboard/timetables">Plany zajęć</a> > <a href="/dashboard/timetable/add">Edytuj plan zajęć</a>'
        )); 
    }
    
    public function subjectEditAction(Request $request, $subject_id) {
        
        $subject = $this->getSubject($subject_id);
        
        $form = $this->createForm(new SubjectType(), $subject);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setUpdateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('subjects');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_edit.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'subjects',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Edytuj przedmiot',
            'path_request' => 'subject',
            'id' => $subject->getId(),
            'top_menu_content' => '> <a href="/dashboard/subjects">Przedmioty</a> > <a href="/dashboard/subject/add">Edytuj przedmiot</a>'
        )); 
    }
    
    public function hourEditAction(Request $request, $hour_id) {
        
        $hour = $this->getHour($hour_id);
        
        $form = $this->createForm(new HourType(), $hour);

        // process the form on POST
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data = $this->setUpdateData($data);
                $this->saveEntity($data);
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $back_url = $this->generateBackUrl('hours');
        return $this->render('mkotlarzQRAppBundle:Dashboard:_edit.html.twig', array(
            'data' => $data,
            'form' => $form->createView(),
            'action' => 'hours',
            'back_url' => $back_url,
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Edytuj godzinę',
            'path_request' => 'hour',
            'id' => $hour->getId(),
            'top_menu_content' => '> <a href="/dashboard/hours">Godziny</a> > <a href="/dashboard/hour/add">Edytuj godzinę</a>'
        )); 
    }
    
    /**
     * Pozwala na dodanie {@link mkotlarz\QRAppBundle\Entity\Replacement} dla {@link mkotlarz\QRAppBundle\Entity\Lesson}
     * @param   integer       $lesson_id
     * @param   Request       $request
     */
    public function replacementsLessonAddAction($lesson_id, Request $request) {
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lesson = $lessons_repository->find($lesson_id);
        $replacement = new Replacement();
        $form = $this->createFormBuilder($replacement)
            ->add('replacement_teacher_id', 'choice', array(
                    'label' => 'Wybierz nauczyciela:',
                    'choices' => $this->generateChoices('mkotlarzQRAppBundle:Teacher'),
                    'attr'   =>  array(
                        'class'   => 'form-control')))
            ->add('replacement_room_id', 'choice', array(
                    'label' => 'Wybierz pomieszczenie:',
                    'choices' => $this->generateChoices('mkotlarzQRAppBundle:Room'),
                    'attr'   =>  array(
                        'class'   => 'form-control')))
            ->add('replacement_subject_id', 'choice', array(
                    'label' => 'Wybierz przedmiot:',
                    'choices' => $this->generateChoices('mkotlarzQRAppBundle:Subject'),
                    'attr'   =>  array(
                        'class'   => 'form-control')))
            ->add('replacement_hour_id', 'choice', array(
                    'label' => 'Wybierz godzinę:',
                    'choices' => $this->generateChoices('mkotlarzQRAppBundle:Hour'),
                    'attr'   =>  array(
                        'class'   => 'form-control')))
            ->add('description', 'textarea', array(
                'label' => 'Opis:',
                'attr' => array(
                    'class' => 'form-control'),
                'required' => false))
            ->add('is_empty', 'checkbox', array(
                    'label' => 'Okienko: ',
                    'required' => false
            ))
            ->add('save', 'submit', array(
                'label' => 'Dodaj zastępstwo',
                'attr' => array(
                    'class' => 'btn btn-success')))
            ->getForm();
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setTimetable($this->getTimetable($lesson->getTimetable()->getId()));
                $data->setHour($this->getHour($lesson->getHour()->getId()));
                $data->setRoom($this->getRoom($lesson->getHour()->getId()));
                $data->setLesson($lesson);
                $data->setTeacher($this->getTeacher($lesson->getTeacher()->getId()));
                $data->setSubject($this->getSubject($lesson->getSubject()->getId()));
                $data = $this->setCreateData($data);
                $this->saveEntity($data);
                return $this->redirect($this->generateUrl('send_push_message', array(
                    'url' => $request->getUri(),
                    'message' => 'Nowe zastępstwo zostało dodane'
                )));
            }
        }
        $data = $this->getRecordsCountFromAllTables();
        $dayofweek = $this->getDayOfWeekFromDatabase($lesson->getDay());
        return $this->render('mkotlarzQRAppBundle:Dashboard:replacementLessonAdd.html.twig', array(
            'data' => $data,
            'action' => 'replacements',
            'lesson' => $lesson,
            'day' => $dayofweek,
            'form' => $form->createView(),
            'user_name' => $data['user_name'],
            'users_number' => $data['users'],
            'rooms_number' => $data['rooms'],
            'hours_number' => $data['hours'],
            'subjects_number' => $data['subjects'],
            'teachers_number' => $data['teachers'],
            'timetables_number' => $data['timetables'],
            'replacements_number' => $data['replacements'],
            'title' => 'Dodaj zastępstwo',
            'top_menu_content' => '> <a href="/dashboard/replacements">Zastęptswa</a> > <a href="/dashboard/replacements/add/' . $lesson_id . '">Dodaj zastępstwo</a>'
        ));
    }

    public function getFreeRoomsAction($time) {
        $freeRooms = $this->getFreeRooms($time);
        return $this->render('mkotlarzQRAppBundle:Dashboard:_freeRooms.html.twig', array(
            'freeRooms' => $freeRooms
        ));
    }

    

    /**
    ***********************************
    *
    * PRIVATE METHODS
    *
    ***********************************
    **/
    
    private function generateChoices($repositoryName) {
        $choices = array();
        $data = $this->getDoctrine()->getRepository($repositoryName)->findAll();

        foreach ($data as $choice) {
            $choices[$choice->getId()] = (string)$choice;
        }


        return $choices;
    }
    
    private function generateToken() {
        $intention = null;
        $csrf = $this->get('form.csrf_provider'); //Symfony\Component\Form\Extension\Csrf\CsrfProvider\SessionCsrfProvider by default
        return $csrf->generateCsrfToken($intention); 
    }
    
    private function getTimetable($timetable_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        return $repository->find($timetable_id);
    }
    
    private function getHour($hour_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Hour');
        return $repository->find($hour_id);
    }
    
    private function getSubject($subject_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Subject');
        return $repository->find($subject_id);
    }
    
    private function getRoom($room_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        return $repository->find($room_id);
    }
    
    private function getTeacher($teacher_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        return $repository->find($teacher_id);
    }
    
    private function getLesson($lesson_id) {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        return $repository->find($lesson_id);
    }
    
    private function saveEntity($data) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
    }
    
    private function getSchool() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:School');
        return $repository->find(1);
    }
    
    private function generateBackUrl($url) {
        return '<a href="/dashboard/' . $url . '">Back</a>';
    }
    
    private function setCreateData($data) {
        $data->setCreatedAt(new \DateTime());
        $data->setUpdatedAt(new \DateTime());   
        return $data;
    }
    
    private function setUpdateData($data) {
        $data->setUpdatedAt(new \DateTime());   
        return $data;
    }
    
    private function getDayOfWeekFromDatabase($day) {
        switch($day) {
            case 1:
                return "Poniedziałek";
            case 2:
                return "Wtorek";
            case 3:
                return "Środa";
            case 4:
                return "Czwartek";
            case 5:
                return "Piątek";
            case 6:
                return "Sobota";
            case 7:
                return "Niedziela";
        }
        return "Poniedziałek";
    }
    
    private function getRecordsCount($table) {
        $repository = $this->getDoctrine()->getRepository($table);
        $result = $repository->getCount();
        return $result;
    }
    
    private function getRecordsCountFromAllTables() {
        $user = $this->getUser();
        $data = array();
        $data['users'] = $this->getRecordsCount('mkotlarzQRAppBundle:User');
        $data['rooms'] = $this->getRecordsCount('mkotlarzQRAppBundle:Room');
        $data['hours'] = $this->getRecordsCount('mkotlarzQRAppBundle:Hour');
        $data['subjects'] = $this->getRecordsCount('mkotlarzQRAppBundle:Subject');
        $data['teachers'] = $this->getRecordsCount('mkotlarzQRAppBundle:Teacher');
        $data['timetables'] = $this->getRecordsCount('mkotlarzQRAppBundle:Timetable');
        $data['replacements'] = $this->getRecordsCount('mkotlarzQRAppBundle:Replacement');
        $data['user_name'] = $user->getName() . ' ' . $user->getSurname();
        $data['user'] = $user;
        return $data;
    }
    
    
    private function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    private function getAllFrom($repository) {
        $repository = $this->getDoctrine()->getRepository($repository);
        return $repository->findAll();
    }

    /**
     * Metoda listująca wszystkie wolne pomieszczenia o zadanej godzinie
     * @param   String          $time   Czas dla którego ma być sprawdzona lista wolnych pomiesczeń
     */
    private function getFreeRooms($time) {
        //Stwórz potrzebne zmienne
        $day = date("N");
        $freeRooms = array();
        $usedRooms = array();
        $checkinFreeRooms = array();
        $i = 0;
        $j = 0;
        $k = 0;
        
        //Sformatuj date ze Stringa do DateTime
        $dateTime = strtotime($time);
        $dateNow = new \DateTime();
        $formatedDate = $dateNow->format('Y-m-d');

        //Pobierz listę pomieszczeń
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $rooms = $repository->findAll();

        //Ustaw repozytorium na Lesson
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');

        //Dodaj nowe repozytorium
        $checkinsRepository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Checkin');

        //Przeleć po wszystkich pomieszczeniach i sprawdź, które jest wolne
        foreach($rooms as $room) {

            //Sprawdź w bazie czy dane poemisczenie jest wolne
            $result = $repository->checkIfRoomIsEmpty($room->getId(), $time, $day);

            //Pobierz checkiny dla pomieszczenia
            $checkins = $checkinsRepository->getTodayCheckinsForRoom($room->getId(), $formatedDate);
            if(!empty($checkins)) {

                //Sprawdź w pętli czy checkin pokrywa się z aktualną/zadaną godziną
                foreach($checkins as $checkin) {
                    $dateStart = strtotime($checkin->getHour()->getStart()->format("H:i"));
                    $dateEnd = strtotime($checkin->getHour()->getEnd()->format("H:i"));
                    if($dateStart < $dateTime && $dateTime < $dateEnd) {
                        if($day == $checkin->getLesson()->getDay()) {
                            $checkinRoom = $checkin->getRoom();
                            $checkinFreeRooms[$k] = $checkin->getLesson()->getRoom();
                            $k++;
                            if(in_array($checkinRoom, $usedRooms))
                                continue;
                            $usedRooms[$i] = $checkinRoom;
                            $i++;
                            break;
                        }
                    }
                }
            }
            //Sprawdź czy pokój znajduje się w innych tablicach, czy jest używany, jeśli nie ma go w żadnej tablicy to dodaj go do $freeRooms
            foreach($result as $lesson) {
                $dateStart = strtotime($lesson->getHour()->getStart()->format("H:i"));
                $dateEnd = strtotime($lesson->getHour()->getEnd()->format("H:i"));
                if($dateStart < $dateTime && $dateTime < $dateEnd) {
                    if(in_array($room, $checkinFreeRooms))
                        break;
                    if(in_array($room, $usedRooms))
                        break;
                    if(in_array($room, $freeRooms))
                        break;
                    $usedRooms[$i] = $room;
                    $i++;
                    break;
                }
            }
            if(in_array($room, $usedRooms))
               continue;
            $freeRooms[$j] = $room;
            $usedRooms[$i] = $room;
            $i++;
            $j++;
        }

        //Sprawdź listę pomieszczeń zwolnionych poprzez zmiane sali lekcyjnej
        //Czy pomieszczenia nadal są wolne ?
        foreach($checkinFreeRooms as $room) {
            if(in_array($room, $usedRooms)) {
                if(($key = array_search($room, $usedRooms)) !== false) {
                    unset($usedRooms[$key]);
                    $freeRooms[$j] = $room;
                    $j++;
                }   
            }
        }
        return $freeRooms;
    }
}