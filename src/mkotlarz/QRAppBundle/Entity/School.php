<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * School
 */
class School
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $postal;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var boolean
     */
    private $isExpired;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return School
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return School
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return School
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return School
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postal
     *
     * @param string $postal
     * @return School
     */
    public function setPostal($postal)
    {
        $this->postal = $postal;

        return $this;
    }

    /**
     * Get postal
     *
     * @return string 
     */
    public function getPostal()
    {
        return $this->postal;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return School
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return School
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isExpired
     *
     * @param boolean $isExpired
     * @return School
     */
    public function setIsExpired($isExpired)
    {
        $this->isExpired = $isExpired;

        return $this;
    }

    /**
     * Get isExpired
     *
     * @return boolean 
     */
    public function getIsExpired()
    {
        return $this->isExpired;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return School
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return School
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \mkotlarz\QRAppBundle\Entity\User $user
     * @return School
     */
    public function setUser(\mkotlarz\QRAppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \mkotlarz\QRAppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $teachers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teachers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teachers
     * @return School
     */
    public function addTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teachers)
    {
        $this->teachers[] = $teachers;

        return $this;
    }

    /**
     * Remove teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teachers
     */
    public function removeTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teachers)
    {
        $this->teachers->removeElement($teachers);
    }

    /**
     * Get teachers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeachers()
    {
        return $this->teachers;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rooms;


    /**
     * Add rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     * @return School
     */
    public function addRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     */
    public function removeRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms->removeElement($rooms);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRooms()
    {
        return $this->rooms;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Hour
     */
    private $hours;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subjects;


    /**
     * Set hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     * @return School
     */
    public function setHours(\mkotlarz\QRAppBundle\Entity\Hour $hours = null)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return \mkotlarz\QRAppBundle\Entity\Hour 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set subjects
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subjects
     * @return School
     */
    public function setSubjects(\mkotlarz\QRAppBundle\Entity\Subject $subjects = null)
    {
        $this->subjects = $subjects;

        return $this;
    }

    /**
     * Get subjects
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubjects()
    {
        return $this->subjects;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $timetables;


    /**
     * Add timetables
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetables
     * @return School
     */
    public function addTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetables)
    {
        $this->timetables[] = $timetables;

        return $this;
    }

    /**
     * Remove timetables
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetables
     */
    public function removeTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetables)
    {
        $this->timetables->removeElement($timetables);
    }

    /**
     * Get timetables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTimetables()
    {
        return $this->timetables;
    }

    /**
     * Add hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     * @return School
     */
    public function addHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours[] = $hours;

        return $this;
    }

    /**
     * Remove hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     */
    public function removeHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours->removeElement($hours);
    }

    /**
     * Add subjects
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subjects
     * @return School
     */
    public function addSubject(\mkotlarz\QRAppBundle\Entity\Subject $subjects)
    {
        $this->subjects[] = $subjects;

        return $this;
    }

    /**
     * Remove subjects
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subjects
     */
    public function removeSubject(\mkotlarz\QRAppBundle\Entity\Subject $subjects)
    {
        $this->subjects->removeElement($subjects);
    }
    /**
     * @var string
     */
    private $token;


    /**
     * Set token
     *
     * @param string $token
     * @return School
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }
    
    public function __toString() {
        return $this->name;
    }
}
