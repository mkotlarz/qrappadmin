<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TeacherRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TeacherRepository extends EntityRepository
{
    public function findFirst() {
        $result = $this->createQueryBuilder('t')
                    ->select('t')
                    ->from('mkotlarzQRAppBundle:Teacher', 'teacher')
                    ->orderBy('teacher.id', 'ASC')
                    ->setMaxResults(1)
                    ->getQuery()->getResult();
        if(!empty($result[0]))
            return $result[0];
        return null;
    }
    
    public function findLastAddedTeachers($limit = 100) {
        return  $this->createQueryBuilder('t')
                    ->select('t')
                    ->setMaxResults($limit)
                    ->orderBy('t.id', 'DESC')
                    ->getQuery()
                    ->getResult();        
    }
    
    public function getCount() {
        return  $this->createQueryBuilder('t')
                    ->select('COUNT(t)')
                    ->getQuery()
                    ->getSingleScalarResult();
    }
}
