<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subject
 */
class Subject
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Subject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rooms;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $teachers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rooms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teachers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     * @return Subject
     */
    public function addRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     */
    public function removeRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms->removeElement($rooms);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Add teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teachers
     * @return Subject
     */
    public function addTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teachers)
    {
        $this->teachers[] = $teachers;

        return $this;
    }

    /**
     * Remove teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teachers
     */
    public function removeTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teachers)
    {
        $this->teachers->removeElement($teachers);
    }

    /**
     * Get teachers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeachers()
    {
        return $this->teachers;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $hours;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $schools;


    /**
     * Add hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     * @return Subject
     */
    public function addHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours[] = $hours;

        return $this;
    }

    /**
     * Remove hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     */
    public function removeHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours->removeElement($hours);
    }

    /**
     * Get hours
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Add schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Subject
     */
    public function addSchool(\mkotlarz\QRAppBundle\Entity\School $schools)
    {
        $this->schools[] = $schools;

        return $this;
    }

    /**
     * Remove schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     */
    public function removeSchool(\mkotlarz\QRAppBundle\Entity\School $schools)
    {
        $this->schools->removeElement($schools);
    }

    /**
     * Get schools
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSchools()
    {
        return $this->schools;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lessons;


    /**
     * Add lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Subject
     */
    public function addLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Subject
     */
    public function setSchools(\mkotlarz\QRAppBundle\Entity\School $schools = null)
    {
        $this->schools = $schools;

        return $this;
    }

    /**
     * Set lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Subject
     */
    public function setLessons(\mkotlarz\QRAppBundle\Entity\Lesson $lessons = null)
    {
        $this->lessons = $lessons;

        return $this;
    }
    
    public function __toString() {
        return $this->name;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $school;


    /**
     * Set school
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $school
     * @return Subject
     */
    public function setSchool(\mkotlarz\QRAppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchool()
    {
        return $this->school;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Subject
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Subject
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $replacements;


    /**
     * Add replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     * @return Subject
     */
    public function addReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements[] = $replacements;

        return $this;
    }

    /**
     * Remove replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     */
    public function removeReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements->removeElement($replacements);
    }

    /**
     * Get replacements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReplacements()
    {
        return $this->replacements;
    }
}
