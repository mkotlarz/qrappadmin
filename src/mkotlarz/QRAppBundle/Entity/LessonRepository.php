<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * LessonRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LessonRepository extends EntityRepository
{
    public function getLessonsOrderedByHours($timetable_id) {
        $test =  $this->createQueryBuilder('l')
                    ->select('l')
                    ->innerJoin('l.hour', 'hour')
                    ->where('l.timetable = ' . $timetable_id)
                    ->orderBy('hour.start', 'ASC')
                    ->addOrderBy('l.day', 'ASC')
                    ->getQuery();
        return $test->getResult();
    }
     
    public function getLessonsWhereTeacherIdOrderedByHours($teacher_id) {
        return $this->createQueryBuilder('l')
                    ->select('l')
                    ->innerJoin('l.hour', 'hour')
                    ->where('l.teacher = ' . $teacher_id)
                    ->orderBy('hour.start', 'ASC')
                    ->addOrderBy('l.day', 'ASC')
                    ->getQuery()->getResult();
    }
    
    public function getLessonsWhereRoomIdOrderedByHours($room_id) {
        $result = $this->createQueryBuilder('lesson')
                    ->select('lesson')
                    ->innerJoin('lesson.hour', 'hour')
                    ->where('lesson.room = ' . $room_id)
                    ->orderBy('hour.start', 'ASC')
                    ->addOrderBy('lesson.day', 'ASC')
                    ->getQuery();
        return $result->getResult();
    }
    
    public function getLessonsByRoomIdTimeTeacherId($room_id, $time, $day,  $teacher_id) {
        $time = \DateTime::createFromFormat('H:i', $time);
        $test = $this->createQueryBuilder('lesson')
                    ->innerJoin('lesson.hour', 'hour')
                    ->where('lesson.room = ' . $room_id . ' AND lesson.teacher = ' . $teacher_id . " AND hour.start <= '" . $time->format("H:i:s") . "' AND hour.end >= '" . $time->format("H:i:s") . "' AND lesson.day = " . $day)
                    ->orderBy('hour.start', 'ASC')
                    ->addOrderBy('lesson.day', 'ASC')->getQuery();
        echo $test->getSql();
        return $test->getResult();
    }
    
    public function checkIfRoomIsEmpty($room_id, $time_now, $day) {
           $result = $this->createQueryBuilder('l')
                        ->select('l')
                        ->innerJoin('l.room', 'room')
                        ->innerJoin('l.hour', 'hour')
                        ->where("l.day = '" . $day . "' AND room.id = '" . $room_id . "'")
                        ->orderBy('l.id', 'ASC')
                        ->getQuery();
        //echo $result->getSql() . '<br />';
        return $result->getResult();
    }
    
    public function getCount() {
        return  $this->createQueryBuilder('t')
                    ->select('COUNT(t)')
                    ->getQuery()
                    ->getSingleScalarResult();
    }
}
