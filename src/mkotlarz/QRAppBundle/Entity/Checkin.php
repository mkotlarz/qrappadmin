<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subject
 */
class Checkin
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $timetableId;

    /**
     * @var integer
     */
    private $teacherId;

    /**
     * @var integer
     */
    private $roomId;

    /**
     * @var integer
     */
    private $hourId;

    /**
     * @var integer
     */
    private $lessonId;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $teacherToken;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Timetable
     */
    private $timetable;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Room
     */
    private $room;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $teacher;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Hour
     */
    private $hour;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Lesson
     */
    private $lesson;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timetableId
     *
     * @param integer $timetableId
     * @return Checkin
     */
    public function setTimetableId($timetableId)
    {
        $this->timetableId = $timetableId;

        return $this;
    }

    /**
     * Get timetableId
     *
     * @return integer 
     */
    public function getTimetableId()
    {
        return $this->timetableId;
    }

    /**
     * Set teacherId
     *
     * @param integer $teacherId
     * @return Checkin
     */
    public function setTeacherId($teacherId)
    {
        $this->teacherId = $teacherId;

        return $this;
    }

    /**
     * Get teacherId
     *
     * @return integer 
     */
    public function getTeacherId()
    {
        return $this->teacherId;
    }

    /**
     * Set roomId
     *
     * @param integer $roomId
     * @return Checkin
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId
     *
     * @return integer 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set hourId
     *
     * @param integer $hourId
     * @return Checkin
     */
    public function setHourId($hourId)
    {
        $this->hourId = $hourId;

        return $this;
    }

    /**
     * Get hourId
     *
     * @return integer 
     */
    public function getHourId()
    {
        return $this->hourId;
    }

    /**
     * Set lessonId
     *
     * @param integer $lessonId
     * @return Checkin
     */
    public function setLessonId($lessonId)
    {
        $this->lessonId = $lessonId;

        return $this;
    }

    /**
     * Get lessonId
     *
     * @return integer 
     */
    public function getLessonId()
    {
        return $this->lessonId;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Checkin
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Checkin
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format('d/m/Y h:m');
    }

    /**
     * Set teacherToken
     *
     * @param string $teacherToken
     * @return Checkin
     */
    public function setTeacherToken($teacherToken)
    {
        $this->teacherToken = $teacherToken;

        return $this;
    }

    /**
     * Get teacherToken
     *
     * @return string 
     */
    public function getTeacherToken()
    {
        return $this->teacherToken;
    }

    /**
     * Set timetable
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetable
     * @return Checkin
     */
    public function setTimetable(Timetable $timetable = null)
    {
        $this->timetable = $timetable;

        return $this;
    }

    /**
     * Get timetable
     *
     * @return \mkotlarz\QRAppBundle\Entity\Timetable 
     */
    public function getTimetable()
    {
        return $this->timetable;
    }

    /**
     * Set room
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $room
     * @return Checkin
     */
    public function setRoom(Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \mkotlarz\QRAppBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set teacher
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teacher
     * @return Checkin
     */
    public function setTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set hour
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hour
     * @return Checkin
     */
    public function setHour(\mkotlarz\QRAppBundle\Entity\Hour $hour = null)
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get hour
     *
     * @return \mkotlarz\QRAppBundle\Entity\Hour 
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Set lesson
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lesson
     * @return Checkin
     */
    public function setLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lesson = null)
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * Get lesson
     *
     * @return \mkotlarz\QRAppBundle\Entity\Lesson 
     */
    public function getLesson()
    {
        return $this->lesson;
    }
}
