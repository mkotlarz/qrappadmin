<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timetable
 */
class Timetable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Timetable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Timetable
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lessons;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $schools;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Timetable
     */
    public function addLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Timetable
     */
    public function setSchools(\mkotlarz\QRAppBundle\Entity\School $schools = null)
    {
        $this->schools = $schools;

        return $this;
    }

    /**
     * Get schools
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchools()
    {
        return $this->schools;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lesson;


    /**
     * Get lesson
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLesson()
    {
        return $this->lesson;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $school;


    /**
     * Set school
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $school
     * @return Timetable
     */
    public function setSchool(\mkotlarz\QRAppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchool()
    {
        return $this->school;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Timetable
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Timetable
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $replacements;


    /**
     * Add replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     * @return Timetable
     */
    public function addReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements[] = $replacements;

        return $this;
    }

    /**
     * Remove replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     */
    public function removeReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements->removeElement($replacements);
    }

    /**
     * Get replacements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReplacements()
    {
        return $this->replacements;
    }
    
    public function __toString() {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $checkins;


    /**
     * Add checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     * @return Timetable
     */
    public function addCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins[] = $checkins;

        return $this;
    }

    /**
     * Remove checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     */
    public function removeCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins->removeElement($checkins);
    }

    /**
     * Get checkins
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheckins()
    {
        return $this->checkins;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $teacher;


    /**
     * Set teacher
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teacher
     * @return Timetable
     */
    public function setTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }
    /**
     * @var string
     */
    private $profile;


    /**
     * Set profile
     *
     * @param string $profile
     * @return Timetable
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
        return $this->profile;
    }
}
