<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Room
 */
class Room
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $description;
    
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Room
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Room
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Room
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $teachers;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $school;


    /**
     * Set teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teachers
     * @return Room
     */
    public function setTeachers(\mkotlarz\QRAppBundle\Entity\Teacher $teachers = null)
    {
        $this->teachers = $teachers;

        return $this;
    }

    /**
     * Get teachers
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * Set school
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $school
     * @return Room
     */
    public function setSchool(\mkotlarz\QRAppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchool()
    {
        return $this->school;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $hours;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hours = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     * @return Room
     */
    public function addHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours[] = $hours;

        return $this;
    }

    /**
     * Remove hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     */
    public function removeHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours->removeElement($hours);
    }

    /**
     * Get hours
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHours()
    {
        return $this->hours;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $schools;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Lesson
     */
    private $lessons;


    /**
     * Set schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Room
     */
    public function setSchools(\mkotlarz\QRAppBundle\Entity\School $schools = null)
    {
        $this->schools = $schools;

        return $this;
    }

    /**
     * Get schools
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchools()
    {
        return $this->schools;
    }

    /**
     * Set lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Room
     */
    public function setLessons(\mkotlarz\QRAppBundle\Entity\Lesson $lessons = null)
    {
        $this->lessons = $lessons;

        return $this;
    }

    /**
     * Get lessons
     *
     * @return \mkotlarz\QRAppBundle\Entity\Lesson 
     */
    public function getLessons()
    {
        return $this->lessons;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rooms;


    /**
     * Add rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $rooms
     * @return Room
     */
    public function addRoom(\mkotlarz\QRAppBundle\Entity\Lesson $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $rooms
     */
    public function removeRoom(\mkotlarz\QRAppBundle\Entity\Lesson $rooms)
    {
        $this->rooms->removeElement($rooms);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRooms()
    {
        return $this->rooms;
    }
    
    public function __toString() {
        return $this->number;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $teacher;


    /**
     * Set teacher
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teacher
     * @return Room
     */
    public function setTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Room
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Room
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $replacements;


    /**
     * Add lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Room
     */
    public function addLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Add replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     * @return Room
     */
    public function addReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements[] = $replacements;

        return $this;
    }

    /**
     * Remove replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     */
    public function removeReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements->removeElement($replacements);
    }

    /**
     * Get replacements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReplacements()
    {
        return $this->replacements;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $checkins;


    /**
     * Add checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     * @return Room
     */
    public function addCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins[] = $checkins;

        return $this;
    }

    /**
     * Remove checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     */
    public function removeCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins->removeElement($checkins);
    }

    /**
     * Get checkins
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheckins()
    {
        return $this->checkins;
    }
}
