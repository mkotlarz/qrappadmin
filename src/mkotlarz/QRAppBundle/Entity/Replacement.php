<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Replacement
 */
class Replacement
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $timetableId;

    /**
     * @var integer
     */
    private $roomId;

    /**
     * @var integer
     */
    private $hourId;

    /**
     * @var integer
     */
    private $teacherId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timetableId
     *
     * @param integer $timetableId
     * @return Replacement
     */
    public function setTimetableId($timetableId)
    {
        $this->timetableId = $timetableId;

        return $this;
    }

    /**
     * Get timetableId
     *
     * @return integer 
     */
    public function getTimetableId()
    {
        return $this->timetableId;
    }

    /**
     * Set roomId
     *
     * @param integer $roomId
     * @return Replacement
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId
     *
     * @return integer 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set hourId
     *
     * @param integer $hourId
     * @return Replacement
     */
    public function setHourId($hourId)
    {
        $this->hourId = $hourId;

        return $this;
    }

    /**
     * Get hourId
     *
     * @return integer 
     */
    public function getHourId()
    {
        return $this->hourId;
    }

    /**
     * Set teacherId
     *
     * @param integer $teacherId
     * @return Replacement
     */
    public function setTeacherId($teacherId)
    {
        $this->teacherId = $teacherId;

        return $this;
    }

    /**
     * Get teacherId
     *
     * @return integer 
     */
    public function getTeacherId()
    {
        return $this->teacherId;
    }
    /**
     * @var integer
     */
    private $subjectId;

    /**
     * @var integer
     */
    private $lessonId;

    /**
     * @var integer
     */
    private $replacementTeacherId;

    /**
     * @var integer
     */
    private $replacementRoomId;

    /**
     * @var integer
     */
    private $replacementHourId;

    /**
     * @var integer
     */
    private $replacementSubjectId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Timetable
     */
    private $timetable;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Room
     */
    private $room;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subject;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $teacher;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Hour
     */
    private $hour;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Lesson
     */
    private $lesson;


    /**
     * Set subjectId
     *
     * @param integer $subjectId
     * @return Replacement
     */
    public function setSubjectId($subjectId)
    {
        $this->subjectId = $subjectId;

        return $this;
    }

    /**
     * Get subjectId
     *
     * @return integer 
     */
    public function getSubjectId()
    {
        return $this->subjectId;
    }

    /**
     * Set lessonId
     *
     * @param integer $lessonId
     * @return Replacement
     */
    public function setLessonId($lessonId)
    {
        $this->lessonId = $lessonId;

        return $this;
    }

    /**
     * Get lessonId
     *
     * @return integer 
     */
    public function getLessonId()
    {
        return $this->lessonId;
    }

    /**
     * Set replacementTeacherId
     *
     * @param integer $replacementTeacherId
     * @return Replacement
     */
    public function setReplacementTeacherId($replacementTeacherId)
    {
        $this->replacementTeacherId = $replacementTeacherId;

        return $this;
    }

    /**
     * Get replacementTeacherId
     *
     * @return integer 
     */
    public function getReplacementTeacherId()
    {
        return $this->replacementTeacherId;
    }

    /**
     * Set replacementRoomId
     *
     * @param integer $replacementRoomId
     * @return Replacement
     */
    public function setReplacementRoomId($replacementRoomId)
    {
        $this->replacementRoomId = $replacementRoomId;

        return $this;
    }

    /**
     * Get replacementRoomId
     *
     * @return integer 
     */
    public function getReplacementRoomId()
    {
        return $this->replacementRoomId;
    }

    /**
     * Set replacementHourId
     *
     * @param integer $replacementHourId
     * @return Replacement
     */
    public function setReplacementHourId($replacementHourId)
    {
        $this->replacementHourId = $replacementHourId;

        return $this;
    }

    /**
     * Get replacementHourId
     *
     * @return integer 
     */
    public function getReplacementHourId()
    {
        return $this->replacementHourId;
    }

    /**
     * Set replacementSubjectId
     *
     * @param integer $replacementSubjectId
     * @return Replacement
     */
    public function setReplacementSubjectId($replacementSubjectId)
    {
        $this->replacementSubjectId = $replacementSubjectId;

        return $this;
    }

    /**
     * Get replacementSubjectId
     *
     * @return integer 
     */
    public function getReplacementSubjectId()
    {
        return $this->replacementSubjectId;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Replacement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set timetable
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetable
     * @return Replacement
     */
    public function setTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetable = null)
    {
        $this->timetable = $timetable;

        return $this;
    }

    /**
     * Get timetable
     *
     * @return \mkotlarz\QRAppBundle\Entity\Timetable 
     */
    public function getTimetable()
    {
        return $this->timetable;
    }

    /**
     * Set room
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $room
     * @return Replacement
     */
    public function setRoom(\mkotlarz\QRAppBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \mkotlarz\QRAppBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set subject
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subject
     * @return Replacement
     */
    public function setSubject(\mkotlarz\QRAppBundle\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set teacher
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teacher
     * @return Replacement
     */
    public function setTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set hour
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hour
     * @return Replacement
     */
    public function setHour(\mkotlarz\QRAppBundle\Entity\Hour $hour = null)
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get hour
     *
     * @return \mkotlarz\QRAppBundle\Entity\Hour 
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Set lesson
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lesson
     * @return Replacement
     */
    public function setLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lesson = null)
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * Get lesson
     *
     * @return \mkotlarz\QRAppBundle\Entity\Lesson 
     */
    public function getLesson()
    {
        return $this->lesson;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Room
     */
    private $replacementRoom;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $replacementSubject;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Hour
     */
    private $replacementHour;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $replacementTeacher;


    /**
     * Set replacementRoom
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $replacementRoom
     * @return Replacement
     */
    public function setReplacementRoom(\mkotlarz\QRAppBundle\Entity\Room $replacementRoom = null)
    {
        $this->replacementRoom = $replacementRoom;

        return $this;
    }

    /**
     * Get replacementRoom
     *
     * @return \mkotlarz\QRAppBundle\Entity\Room 
     */
    public function getReplacementRoom()
    {
        return $this->replacementRoom;
    }

    /**
     * Set replacementSubject
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $replacementSubject
     * @return Replacement
     */
    public function setReplacementSubject(\mkotlarz\QRAppBundle\Entity\Subject $replacementSubject = null)
    {
        $this->replacementSubject = $replacementSubject;

        return $this;
    }

    /**
     * Get replacementSubject
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getReplacementSubject()
    {
        return $this->replacementSubject;
    }

    /**
     * Set replacementHour
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $replacementHour
     * @return Replacement
     */
    public function setReplacementHour(\mkotlarz\QRAppBundle\Entity\Hour $replacementHour = null)
    {
        $this->replacementHour = $replacementHour;

        return $this;
    }

    /**
     * Get replacementHour
     *
     * @return \mkotlarz\QRAppBundle\Entity\Hour 
     */
    public function getReplacementHour()
    {
        return $this->replacementHour;
    }

    /**
     * Set replacementTeacher
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $replacementTeacher
     * @return Replacement
     */
    public function setReplacementTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $replacementTeacher = null)
    {
        $this->replacementTeacher = $replacementTeacher;

        return $this;
    }

    /**
     * Get replacementTeacher
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getReplacementTeacher()
    {
        return $this->replacementTeacher;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Replacement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Replacement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }
    /**
     * @var boolean
     */
    private $isEmpty;


    /**
     * Set isEmpty
     *
     * @param boolean $isEmpty
     * @return Replacement
     */
    public function setIsEmpty($isEmpty)
    {
        $this->isEmpty = $isEmpty;

        return $this;
    }

    /**
     * Get isEmpty
     *
     * @return boolean 
     */
    public function getIsEmpty()
    {
        return $this->isEmpty;
    }
}
