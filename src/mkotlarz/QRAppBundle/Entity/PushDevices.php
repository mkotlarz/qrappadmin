<?php

namespace mkotlarz\QRAppBundle\Entity;

/**
 * PushDevices
 */
class PushDevices
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $deviceName;

    /**
     * @var string
     */
    private $deviceId;

    /**
     * @var string
     */
    private $country;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     *
     * @return PushDevices
     */
    public function setDeviceName($deviceName)
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     *
     * @return PushDevices
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return PushDevices
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
}

