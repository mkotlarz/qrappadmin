<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Teacher
 */
class Teacher
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $postal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Teacher
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Teacher
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Teacher
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Teacher
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postal
     *
     * @param string $postal
     * @return Teacher
     */
    public function setPostal($postal)
    {
        $this->postal = $postal;

        return $this;
    }

    /**
     * Get postal
     *
     * @return string 
     */
    public function getPostal()
    {
        return $this->postal;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $school;


    /**
     * Set school
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $school
     * @return Teacher
     */
    public function setSchool(\mkotlarz\QRAppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchool()
    {
        return $this->school;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rooms;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rooms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     * @return Teacher
     */
    public function addRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     */
    public function removeRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms->removeElement($rooms);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRooms()
    {
        return $this->rooms;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subjects;


    /**
     * Set subjects
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subjects
     * @return Teacher
     */
    public function setSubjects(\mkotlarz\QRAppBundle\Entity\Subject $subjects = null)
    {
        $this->subjects = $subjects;

        return $this;
    }

    /**
     * Get subjects
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubjects()
    {
        return $this->subjects;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Lesson
     */
    private $lessons;


    /**
     * Set lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Teacher
     */
    public function setLessons(\mkotlarz\QRAppBundle\Entity\Lesson $lessons = null)
    {
        $this->lessons = $lessons;

        return $this;
    }

    /**
     * Get lessons
     *
     * @return \mkotlarz\QRAppBundle\Entity\Lesson 
     */
    public function getLessons()
    {
        return $this->lessons;
    }
    /**
     * @var string
     */
    private $token;


    /**
     * Set token
     *
     * @param string $token
     * @return Teacher
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }
    
    public function __toString() {
        return $this->getName() . " " . $this->getSurname();
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $schools;


    /**
     * Set schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Teacher
     */
    public function setSchools(\mkotlarz\QRAppBundle\Entity\School $schools = null)
    {
        $this->schools = $schools;

        return $this;
    }

    /**
     * Get schools
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchools()
    {
        return $this->schools;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $teachers;


    /**
     * Add teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $teachers
     * @return Teacher
     */
    public function addTeacher(\mkotlarz\QRAppBundle\Entity\Lesson $teachers)
    {
        $this->teachers[] = $teachers;

        return $this;
    }

    /**
     * Remove teachers
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $teachers
     */
    public function removeTeacher(\mkotlarz\QRAppBundle\Entity\Lesson $teachers)
    {
        $this->teachers->removeElement($teachers);
    }

    /**
     * Get teachers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeachers()
    {
        return $this->teachers;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subject;


    /**
     * Set subject
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subject
     * @return Teacher
     */
    public function setSubject(\mkotlarz\QRAppBundle\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubject()
    {
        return $this->subject;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Teacher
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Teacher
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $replacements;


    /**
     * Add lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Teacher
     */
    public function addLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Add replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     * @return Teacher
     */
    public function addReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements[] = $replacements;

        return $this;
    }

    /**
     * Remove replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     */
    public function removeReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements->removeElement($replacements);
    }

    /**
     * Get replacements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReplacements()
    {
        return $this->replacements;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $checkins;


    /**
     * Add checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     * @return Teacher
     */
    public function addCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins[] = $checkins;

        return $this;
    }

    /**
     * Remove checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     */
    public function removeCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins->removeElement($checkins);
    }

    /**
     * Get checkins
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheckins()
    {
        return $this->checkins;
    }
    /**
     * @var string
     */
    private $email;


    /**
     * Set email
     *
     * @param string $email
     * @return Teacher
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Timetable
     */
    private $timetable;


    /**
     * Set timetable
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetable
     * @return Teacher
     */
    public function setTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetable = null)
    {
        $this->timetable = $timetable;

        return $this;
    }

    /**
     * Get timetable
     *
     * @return \mkotlarz\QRAppBundle\Entity\Timetable 
     */
    public function getTimetable()
    {
        return $this->timetable;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $timetables;


    /**
     * Add timetables
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetables
     * @return Teacher
     */
    public function addTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetables)
    {
        $this->timetables[] = $timetables;

        return $this;
    }

    /**
     * Remove timetables
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetables
     */
    public function removeTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetables)
    {
        $this->timetables->removeElement($timetables);
    }

    /**
     * Get timetables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTimetables()
    {
        return $this->timetables;
    }

    public function getExtra() {
        return $this->name[0] . "" . $this->surname[0];
    }
}
