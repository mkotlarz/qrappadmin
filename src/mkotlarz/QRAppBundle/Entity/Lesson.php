<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lesson
 */
class Lesson
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $day;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param string $day
     * @return Lesson
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return string 
     */
    public function getDay()
    {
        return $this->day;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $hours;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Timetable
     */
    private $timetables;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subjects;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Room
     */
    private $rooms;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hours = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     * @return Lesson
     */
    public function addHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours[] = $hours;

        return $this;
    }

    /**
     * Remove hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     */
    public function removeHour(\mkotlarz\QRAppBundle\Entity\Hour $hours)
    {
        $this->hours->removeElement($hours);
    }

    /**
     * Get hours
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set timetables
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetables
     * @return Lesson
     */
    public function setTimetables(\mkotlarz\QRAppBundle\Entity\Timetable $timetables = null)
    {
        $this->timetables = $timetables;

        return $this;
    }

    /**
     * Get timetables
     *
     * @return \mkotlarz\QRAppBundle\Entity\Timetable 
     */
    public function getTimetables()
    {
        return $this->timetables;
    }

    /**
     * Set subjects
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subjects
     * @return Lesson
     */
    public function setSubjects(\mkotlarz\QRAppBundle\Entity\Subject $subjects = null)
    {
        $this->subjects = $subjects;

        return $this;
    }

    /**
     * Get subjects
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubjects()
    {
        return $this->subjects;
    }

    /**
     * Set rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     * @return Lesson
     */
    public function setRooms(\mkotlarz\QRAppBundle\Entity\Room $rooms = null)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return \mkotlarz\QRAppBundle\Entity\Room 
     */
    public function getRooms()
    {
        return $this->rooms;
    }
    
    /**
     * Add rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     * @return Lesson
     */
    public function addRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $rooms
     */
    public function removeRoom(\mkotlarz\QRAppBundle\Entity\Room $rooms)
    {
        $this->rooms->removeElement($rooms);
    }

    /**
     * Set hours
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hours
     * @return Lesson
     */
    public function setHours(\mkotlarz\QRAppBundle\Entity\Hour $hours = null)
    {
        $this->hours = $hours;

        return $this;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Timetable
     */
    private $timetable;


    /**
     * Set timetable
     *
     * @param \mkotlarz\QRAppBundle\Entity\Timetable $timetable
     * @return Lesson
     */
    public function setTimetable(\mkotlarz\QRAppBundle\Entity\Timetable $timetable = null)
    {
        $this->timetable = $timetable;

        return $this;
    }

    /**
     * Get timetable
     *
     * @return \mkotlarz\QRAppBundle\Entity\Timetable 
     */
    public function getTimetable()
    {
        return $this->timetable;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Room
     */
    private $room;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subject;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Hour
     */
    private $hour;
    
    


    /**
     * Set room
     *
     * @param \mkotlarz\QRAppBundle\Entity\Room $room
     * @return Lesson
     */
    public function setRoom(\mkotlarz\QRAppBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \mkotlarz\QRAppBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set subject
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subject
     * @return Lesson
     */
    public function setSubject(\mkotlarz\QRAppBundle\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set hour
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $hour
     * @return Lesson
     */
    public function setHour(\mkotlarz\QRAppBundle\Entity\Hour $hour = null)
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get hour
     *
     * @return \mkotlarz\QRAppBundle\Entity\Hour 
     */
    public function getHour()
    {
        return $this->hour;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Teacher
     */
    private $teacher;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Lesson
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Lesson
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('d/m/Y h:m');
    }

    /**
     * Set teacher
     *
     * @param \mkotlarz\QRAppBundle\Entity\Teacher $teacher
     * @return Lesson
     */
    public function setTeacher(\mkotlarz\QRAppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \mkotlarz\QRAppBundle\Entity\Teacher 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $replacements;


    /**
     * Add replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     * @return Lesson
     */
    public function addReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements[] = $replacements;

        return $this;
    }

    /**
     * Remove replacements
     *
     * @param \mkotlarz\QRAppBundle\Entity\Replacement $replacements
     */
    public function removeReplacement(\mkotlarz\QRAppBundle\Entity\Replacement $replacements)
    {
        $this->replacements->removeElement($replacements);
    }

    /**
     * Get replacements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReplacements()
    {
        return $this->replacements;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $checkins;


    /**
     * Add checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     * @return Lesson
     */
    public function addCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins[] = $checkins;

        return $this;
    }

    /**
     * Remove checkins
     *
     * @param \mkotlarz\QRAppBundle\Entity\Checkin $checkins
     */
    public function removeCheckin(\mkotlarz\QRAppBundle\Entity\Checkin $checkins)
    {
        $this->checkins->removeElement($checkins);
    }

    /**
     * Get checkins
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheckins()
    {
        return $this->checkins;
    }
}
