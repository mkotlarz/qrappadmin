function getCloudStorageHtml() {
    return '<p class="feature-detail-logo"><i class="fa fa-cloud"></i></p><p class="feature-detail-header">Cloud storage</p><p class="feature-detail-subtext"> N da sdjka sdasdaksdasjkd asd asjd askd akjd as da dhka sda skjd askd ajks dajk aks djka djka sdjkas dkjasdjkansdja sjkdasdua dbakd asjkd aks</p>';
} 

function getManageRulesHtml() {
    return '<p class="feature-detail-logo"><i class="fa fa-cogs"></i></p><p class="feature-detail-header">Manage rules</p><p class="feature-detail-subtext"> N da sdjka sdasdaksdasjkd asd asjd askd akjd as da dhka sda skjd askd ajks dajk aks djka djka sdjkas dkjasdjkansdja sjkdasdua dbakd asjkd aks</p>';
} 

function getAllPhonesHtml() {
    return '<p class="feature-detail-logo"><i class="fa fa-users"></i></p><p class="feature-detail-header">All phones</p><p class="feature-detail-subtext"> N da sdjka sdasdaksdasjkd asd asjd askd akjd as da dhka sda skjd askd ajks dajk aks djka djka sdjkas dkjasdjkansdja sjkdasdua dbakd asjkd aks</p>';
} 

function getSetAutoresponderHtml() {
    return '<p class="feature-detail-logo"><i class="fa fa-share"></i></p><p class="feature-detail-header">Set Autoresponder</p><p class="feature-detail-subtext"> N da sdjka sdasdaksdasjkd asd asjd askd akjd as da dhka sda skjd askd ajks dajk aks djka djka sdjkas dkjasdjkansdja sjkdasdua dbakd asjkd aks</p>';
} 

$( ".list-item" ).click(function() {
    removeListItemSelectedClass();
    $('#feature-details').empty();
    var clicked_item = $(this);
    setOnClickListItem(clicked_item);
});

function removeListItemSelectedClass() {
    var active_item = $('.list-item-selected').first();
    active_item.removeClass('list-item-selected');
    active_item.addClass('list-item');
    active_item.click(function() {
        removeListItemSelectedClass();
        var feature_details = $('#feature-details');
        feature_details.empty();
        var clicked_item = $(this);
        setOnClickListItem(clicked_item);
    })
}

function setOnClickListItem(clicked_item) {
        var feature_details = $('#feature-details');
        feature_details.empty();
        clicked_item.removeClass("list-item");
        clicked_item.addClass("list-item-selected");
        var rel_value = clicked_item.attr('rel').toString();
        if(rel_value == "cloud_storage") {
            feature_details.html(getCloudStorageHtml());
        }
        if(rel_value == "manage_rules") {
            feature_details.html(getManageRulesHtml());
        }
        if(rel_value == "all_phones") {
            feature_details.html(getAllPhonesHtml());
        }
        if(rel_value == "set_autoresponder") {
            feature_details.html(getSetAutoresponderHtml());   
        }
}

$(document).ready(function(){
    jQuery('.bxslider').bxSlider({
    mode: 'horizontal',
	speed: 500,
	slideMargin:20,
	infiniteLoop: true,
    pager: false,
	controls: true,
    slideWidth: 296 ,
    minSlides: 1,
    maxSlides: 3,
    moveSlides: 1  		});
});

