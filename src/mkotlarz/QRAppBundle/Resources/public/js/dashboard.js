$( document ).ready(function() {

	updateRoomsListAjax(new Date);

	$( "#select_custom_time" ).click(function() {
		html1 = getHtmlCustomTimeSelectHours();
		html2 = getHtmlCustomTimeSelectMinutes();
		html3 = getHtmlSubmitButton();
		$("#show_free_rooms_header").removeClass("info-tile-header");
		$("#show_free_rooms_header").addClass("info-tile-header-no-font");
		$("#show_free_rooms_header").html(html1 + '<strong style="float:left;">:</strong>' + html2 + html3);
			$("#submit_custom_time").click(function() {
				hour = $("#select_box_custom_time_hour").val();
				minutes = $("#select_box_custom_time_minutes").val();
				$("#free_rooms").empty();
				updateRoomsListAjax(hour + ":" + minutes);
				restoreFreeRoomsTileHeader(hour + ":" + minutes);
			});
	});

});

function updateRoomsListAjax(date) {
    $.ajax({
		  url: "/dashboard/free_rooms/" + date,
		  context: document.body
		}).done(function(data) {
			$("#free_rooms").empty();
		  	$("#free_rooms").html(data);
		});
}

function restoreFreeRoomsTileHeader(date) {
	$("#show_free_rooms_header").html('Lista wolnych pomieszczeń o godzinie: ' + date + '<button class="btn btn-primary" id="select_custom_time">Wybierz godzinę</button>');
	$("#show_free_rooms_header").addClass("info-tile-header");
	$("#show_free_rooms_header").removeClass("info-tile-header-no-font");
	$( "#select_custom_time" ).click(function() {
		html1 = getHtmlCustomTimeSelectHours();
		html2 = getHtmlCustomTimeSelectMinutes();
		html3 = getHtmlSubmitButton();
		$("#show_free_rooms_header").removeClass("info-tile-header");
		$("#show_free_rooms_header").addClass("info-tile-header-no-font");
		$("#show_free_rooms_header").html(html1 + '<strong style="float:left;">:</strong>' + html2 + html3);
			$("#submit_custom_time").click(function() {
				hour = $("#select_box_custom_time_hour").val();
				minutes = $("#select_box_custom_time_minutes").val();
				$("#free_rooms").empty();
				updateRoomsListAjax(hours+ ";" + minutes);
			});
	});

}

function getHtmlCustomTimeSelectHours() {
	return '<select id="select_box_custom_time_hour" class="form-control" style="width: 70px; float: left;">'+
	'<option value="0">0</option>' +
	'<option value="1">1</option>' +
	'<option value="2">2</option>' +
	'<option value="3">3</option>' +
	'<option value="4">4</option>' +
	'<option value="5">5</option>' +
	'<option value="6">6</option>' +
	'<option value="7">7</option>' +
	'<option value="8">8</option>' +
	'<option value="9">9</option>' +
	'<option value="10">10</option>' +
	'<option value="11">11</option>' +
	'<option value="12">12</option>' +
	'<option value="13">13</option>' +
	'<option value="14">14</option>' +
	'<option value="15">15</option>' +
	'<option value="16">16</option>' +
	'<option value="17">17</option>' +
	'<option value="18">18</option>' +
	'<option value="19">19</option>' +
	'<option value="20">20</option>' +
	'<option value="21">21</option>' +
	'<option value="22">22</option>' +
	'<option value="23">23</option>' +
	'</select>';
}

function getHtmlCustomTimeSelectMinutes() {
	return '<select id="select_box_custom_time_minutes" class="form-control" style="width: 70px; float: left;">'+
	'<option value="0">0</option>' +
	'<option value="1">1</option>' +
	'<option value="2">2</option>' +
	'<option value="3">3</option>' +
	'<option value="4">4</option>' +
	'<option value="5">5</option>' +
	'<option value="6">6</option>' +
	'<option value="7">7</option>' +
	'<option value="8">8</option>' +
	'<option value="9">9</option>' +
	'<option value="10">10</option>' +
	'<option value="11">11</option>' +
	'<option value="12">12</option>' +
	'<option value="23">23</option>' +
	'<option value="24">24</option>' +
	'<option value="25">25</option>' +
	'<option value="26">26</option>' +
	'<option value="27">27</option>' +
	'<option value="28">28</option>' +
	'<option value="29">29</option>' +
	'<option value="30">30</option>' +
	'<option value="31">31</option>' +
	'<option value="32">32</option>' +
	'<option value="33">33</option>' +
	'<option value="34">34</option>' +
	'<option value="35">35</option>' +
	'<option value="36">36</option>' +
	'<option value="37">37</option>' +
	'<option value="38">38</option>' +
	'<option value="39">39</option>' +
	'<option value="40">40</option>' +
	'<option value="41">41</option>' +
	'<option value="42">42</option>' +
	'<option value="43">43</option>' +
	'<option value="44">44</option>' +
	'<option value="45">45</option>' +
	'<option value="46">46</option>' +
	'<option value="47">47</option>' +
	'<option value="48">48</option>' +
	'<option value="49">49</option>' +
	'<option value="50">50</option>' +
	'<option value="51">51</option>' +
	'<option value="52">52</option>' +
	'<option value="53">53</option>' +
	'<option value="54">54</option>' +
	'<option value="55">55</option>' +
	'<option value="56">56</option>' +
	'<option value="57">57</option>' +
	'<option value="58">58</option>' +
	'<option value="59">59</option>' +
	'</select>';
}

function getHtmlSubmitButton() {
	return 	'<button style="margin-left: 15px;" class="btn btn-success" id="submit_custom_time">Wybierz godzinę</button>';
}