<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class HourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('number', 'number', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        $builder->add('start', 'time', array());
        $builder->add('end', 'time', array());
        $builder->add('save', 'submit', array(
            'label' => 'Dodaj godzinę',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Hour',
        );
    }

    public function getName()
    {
        return 'hour';
    }
}