<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class TimetableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        $builder->add('teacher', 'entity', array(
        'label' => 'Wybierz wychowawce:',
        'class'  => 'mkotlarzQRAppBundle:Teacher', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        $builder->add('profile', 'text', array(
        'label' => 'Profil: ',
        'attr'   =>  array(
                'class'   => 'form-control')));
        $builder->add('save', 'submit', array(
            'label' => 'Create Hour',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Hour',
        );
    }

    public function getName()
    {
        return 'hour';
    }
}