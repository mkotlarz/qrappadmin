<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class LessonTeacherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
                
        $builder->add('timetable', 'entity', array(
        'class'  => 'mkotlarzQRAppBundle:Timetable', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('subject', 'entity', array(
        'class'  => 'mkotlarzQRAppBundle:Subject', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('hour', 'entity', array(
        'class'  => 'mkotlarzQRAppBundle:Hour', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('room', 'entity', array(
        'class'  => 'mkotlarzQRAppBundle:Room', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('day', 'choice', array(
            'choices' => array(
                '1'    => 'Monday',
                '2'   => 'Tuesday',
                '3' => 'Wednesday',
                '4'  => 'Thursday',
                '5'    => 'Friday',
                '6'  => 'Saturday',
                '7'    => 'Sunday'
            ),
            
            'attr'   =>  array(
                'class'   => 'form-control')
        ));
        
        $builder->add('save', 'submit', array(
            'label' => 'Create Room',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Room',
        );
    }

    public function getName()
    {
        return 'room';
    }
}