<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class SubjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('save', 'submit', array(
            'label' => 'Create Subject',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Room',
        );
    }

    public function getName()
    {
        return 'room';
    }
}