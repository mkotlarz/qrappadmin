<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class SchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('email', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('city', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('street', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('postal', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('country', 'country', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('save', 'submit', array(
            'label' => 'Create Hour',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Hour',
        );
    }

    public function getName()
    {
        return 'hour';
    }
}