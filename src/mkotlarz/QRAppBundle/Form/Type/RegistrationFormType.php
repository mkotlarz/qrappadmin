<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        $builder->remove('username');
        $builder->add('name', 'text', array(
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('surname', 'text', array(
            'attr' => array(
                'class' => 'form-control'
            )));
        $builder->add('city', 'text', array(
            'attr' => array(
                'class' => 'form-control'
            )));
        $builder->add('street', 'text', array(
            'attr' => array(
                'class' => 'form-control'
            )));
        $builder->add('postal', 'text', array(
            'attr' => array(
                'class' => 'form-control'
            )));
        $builder->add('phone_number', 'text', array(
            'attr' => array(
                'class' => 'form-control'
            )));
        $builder->add('country', 'country', array(
            'attr' => array(
                'class' => 'form-control'
        )));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'mkotlarz_user_registration';
    }
}