<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangePasswordType extends AbstractType
{
    private $class;

    /**
     * @param string $class The User class name
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('current_password', 'password', array(
            'mapped' => false,
            'label' => 'Aktualne hasło',
            'constraints' => new UserPassword(),
            'attr' => array(
                'class' => 'form-control'   
            )
        ));
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array(
                'label' => 'Hasło',
                'attr' => array(
                    'class' => 'form-control'   
                )
                                    ),
            'second_options' => array(
                'label' => 'Powtórz hasło',
                'attr' => array(
                    'class' => 'form-control'   
                )
            ),
            'invalid_message' => 'Hasła się różnią',
        ));
    }
    
    public function getParent()
    {
        return 'fos_user_change_password';
    }

    public function getName()
    {
        return 'mkotlarz_user_change_password';
    }
}
