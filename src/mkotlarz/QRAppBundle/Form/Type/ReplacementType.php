<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ReplacementType extends AbstractType
{
    
    private $doctrine = null;
    
    public function __construct($doc) {
        $this->doctrine = $doc;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('timetable', 'entity', array(
        'label' => 'Wybierz klasę:',
        'class'  => 'mkotlarzQRAppBundle:Timetable', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('hour', 'entity', array(
        'label' => 'Wybierz godzinę:',
        'class'  => 'mkotlarzQRAppBundle:Hour', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('teacher', 'entity', array(
        'label' => 'Wybierz nauczyciela:',
        'class'  => 'mkotlarzQRAppBundle:Teacher', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('replacement_teacher_id', 'choice', array(
        'label' => 'Wybierz nauczyciela dla zastępstwa:',
        'choices' => $this->generateChoices('mkotlarzQRAppBundle:Teacher'),
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('replacement_room_id', 'choice', array(
        'label' => 'Wybierz pomieszczenie dla zastępstwa:',
        'choices' => $this->generateChoices('mkotlarzQRAppBundle:Room'),
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('replacement_subject_id', 'choice', array(
        'label' => 'Wybierz przedmiot dla zastępstwa:',
        'choices' => $this->generateChoices('mkotlarzQRAppBundle:Subject'),
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('description', 'textarea', array(
        'label' => 'Wybierz opis:',
        'required' => false,
        'attr'   =>  array(
                'class'   => 'form-control')));      
        
        $builder->add('is_empty', 'checkbox', array(
            'label' => 'Okienko: '
        ));
        
        
                
        $builder->add('save', 'submit', array(
            'label' => 'Create Room',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Replacement',
        );
    }
    
    private function generateChoices($repositoryName) {
        $choices = array();
        $data = $this->doctrine->getRepository($repositoryName)->findAll();

        foreach ($data as $choice) {
            $choices[$choice->getId()] = (string)$choice;
        }


        return $choices;
    }
    
    public function getName()
    {
        return 'replacement';
    }
}