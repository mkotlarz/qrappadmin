<?php

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('number', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('description', 'text', array(
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('teacher', 'entity', array(
        'class'  => 'mkotlarzQRAppBundle:Teacher', 
        'attr'   =>  array(
                'class'   => 'form-control')));
        
        $builder->add('save', 'submit', array(
            'label' => 'Create Room',
            'attr'   =>  array(
                'class'   => 'btn btn-success')));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'mkotlarz\QRAppBundle\Entity\Room',
        );
    }

    public function getName()
    {
        return 'room';
    }
}