<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace mkotlarz\QRAppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ProfileType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('username', 'hidden', array());
        
        $builder
            ->add('plainPassword', 'hidden', array());
        
        $builder
            ->add('email', 'email', array(
                'label' => 'Adres email',
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
        
        $builder
            ->add('name', 'text', array(
                'label' => 'Imię',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'profile_name'
                )
            ));
        
        $builder
            ->add('surname', 'text', array(
                'label' => 'Nazwisko',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'profile_surname'
                )
            ));
        
        $builder
            ->add('street', 'text', array(
                'label' => 'Ulica',
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
        
        $builder
            ->add('postal', 'text', array(
                'label' => 'Kod pocztowy',
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
        
        $builder
            ->add('phone_number', 'text', array(
                'label' => 'Numer telefonu',
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'mkotlarz_user_edit';
    }

}
