<?php

namespace mkotlarz\QRAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hour
 */
class Hour
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $start;

    /**
     * @var string
     */
    private $end;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return hour
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set start
     *
     * @param string $start
     * @return Hour
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return string 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param string $end
     * @return Hour
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return string 
     */
    public function getEnd()
    {
        return $this->end;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $schools;

    /**
     * @var \mkotlarz\QRAppBundle\Entity\Subject
     */
    private $subjects;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schools = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Hour
     */
    public function addSchool(\mkotlarz\QRAppBundle\Entity\School $schools)
    {
        $this->schools[] = $schools;

        return $this;
    }

    /**
     * Remove schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     */
    public function removeSchool(\mkotlarz\QRAppBundle\Entity\School $schools)
    {
        $this->schools->removeElement($schools);
    }

    /**
     * Get schools
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSchools()
    {
        return $this->schools;
    }

    /**
     * Set subjects
     *
     * @param \mkotlarz\QRAppBundle\Entity\Subject $subjects
     * @return Hour
     */
    public function setSubjects(\mkotlarz\QRAppBundle\Entity\Subject $subjects = null)
    {
        $this->subjects = $subjects;

        return $this;
    }

    /**
     * Get subjects
     *
     * @return \mkotlarz\QRAppBundle\Entity\Subject 
     */
    public function getSubjects()
    {
        return $this->subjects;
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\Hour
     */
    private $rooms;


    /**
     * Set rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Hour $rooms
     * @return Hour
     */
    public function setRooms(\mkotlarz\QRAppBundle\Entity\Hour $rooms = null)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return \mkotlarz\QRAppBundle\Entity\Hour 
     */
    public function getRooms()
    {
        return $this->rooms;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lessons;


    /**
     * Add lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Hour
     */
    public function addLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\mkotlarz\QRAppBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set lessons
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $lessons
     * @return Hour
     */
    public function setLessons(\mkotlarz\QRAppBundle\Entity\Lesson $lessons = null)
    {
        $this->lessons = $lessons;

        return $this;
    }

    /**
     * Set schools
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $schools
     * @return Hour
     */
    public function setSchools(\mkotlarz\QRAppBundle\Entity\School $schools = null)
    {
        $this->schools = $schools;

        return $this;
    }

    /**
     * Add rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $rooms
     * @return Hour
     */
    public function addRoom(\mkotlarz\QRAppBundle\Entity\Lesson $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \mkotlarz\QRAppBundle\Entity\Lesson $rooms
     */
    public function removeRoom(\mkotlarz\QRAppBundle\Entity\Lesson $rooms)
    {
        $this->rooms->removeElement($rooms);
    }
    
    public function __toString() {
        return $this->start . " - " . $this->end;   
    }
    /**
     * @var \mkotlarz\QRAppBundle\Entity\School
     */
    private $school;


    /**
     * Set school
     *
     * @param \mkotlarz\QRAppBundle\Entity\School $school
     * @return Hour
     */
    public function setSchool(\mkotlarz\QRAppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \mkotlarz\QRAppBundle\Entity\School 
     */
    public function getSchool()
    {
        return $this->school;
    }
}

