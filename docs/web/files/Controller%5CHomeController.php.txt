<?php

namespace mkotlarz\QRAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    /**
     * Wyświetla landing page
     */
    public function landingPageAction() 
	{
		return $this->render('mkotlarzQRAppBundle:Home:landing_page.html.twig');
	}
}

