<?php

namespace mkotlarz\QRAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class JSONGeneratorController extends Controller
{
    /**
     * Generuje JSON z lekcjami dla {@link mkotlarz\QRAppBundle\Entity\Timetable}
     * @param   Number $school_id    
     * @param   Number $timetable_id 
     */
    public function generateTimetableJSONAction($school_id, $timetable_id)
    {   
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Timetable');
        $timetable = $repository->find($timetable_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsOrderedByHours($timetable->getId());
        $json = $this->generateJson($lessons);
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /**
     * Generuje JSON z lekcjami dla {@link mkotlarz\QRAppBundle\Entity\Teacher}
     * @param   Number $school_id  
     * @param   Number $teacher_id 
     */
    public function generateTeacherJSONAction($school_id, $teacher_id)
    {   
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Teacher');
        $teacher = $repository->find($teacher_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereTeacherIdOrderedByHours($teacher->getId());
        $json = $this->generateJson($lessons);
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /**
     * Generuje JSON z lekcjami dla {@link mkotlarz\QRAppBundle\Entity\Room}
     * @param   Number $school_id 
     * @param   Number $room_id   
     */
    public function generateRoomJSONAction($school_id, $room_id)
    {   
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Room');
        $room = $repository->find($room_id);
        $lessons_repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:Lesson');
        $lessons = $lessons_repository->getLessonsWhereRoomIdOrderedByHours($room->getId());
        $json = $this->generateJson($lessons);
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
        
    /**
     * Enumeruje plany zajęc {@link mkotlarz\QRAppBundle\Entity\Timetable} do JSON
     */
    public function enumerateTimetablesJSONAction() {
        $timetables = $this->getAllFromRepository('mkotlarzQRAppBundle:Timetable');
        $json = '{ "data": {"timetables": [';
        $i = 0;
        foreach ($timetables as $timetable) {
            if($i !=0)
                $json .=", ";
            $json .= '{';
            $json .= '"timetable_id": "' . $timetable->getId() . '",
                      "timetable": "' . $timetable->getName() . '"
                      }';
            $i++;
        }
        $json .= "]}}";
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /**
     * Enumeruje plany zajęc {@link mkotlarz\QRAppBundle\Entity\Teacher} do JSON
     *  String JSON
     */
    public function enumerateTeachersJSONAction() {
        $teachers = $this->getAllFromRepository('mkotlarzQRAppBundle:Teacher');
        $json = '{ "data": { "teachers": [';
        $i = 0;
        foreach ($teachers as $teacher) {
            if($i !=0)
                $json .=", ";
            $json .= '{';
            $json .= '"teacher_id": "' . $teacher->getId() . '",
                      "teacher": "' . (string)$teacher . '"
                      }';
            $i++;
        }
        $json .= "]}}";
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
     /**
     * Enumeruje plany zajęc {@link mkotlarz\QRAppBundle\Entity\Room} do JSON
     *  String JSON
     */
    public function enumerateRoomsJSONAction() {
        $rooms = $this->getAllFromRepository('mkotlarzQRAppBundle:Room');
        $json = '{ "data": {"rooms": [';
        $i = 0;
        foreach ($rooms as $room) {
            if($i !=0)
                $json .=", ";
            $json .= '{';
            $json .= '"room_id": "' . $room->getId() . '",
                      "room": "' . $room->getNumber() . '"
                      }';
            $i++;
        }
        $json .= "]}}";
        return $this->render('mkotlarzQRAppBundle:JSONGenerator:_base.html.twig', array('json' => $json));
    }
    
    /*
     ***********************************************
     
     Private methods
     
     ***********************************************
     */
    
    /**
    *   Generuje JSON z lekcjami oraz zastępstwami
    */
    private function generateJson($lessons) {
        $school = $this->getSchool();
        $json =  '{"data": { "school_name": "' . $school->getName() . '",';
        $json .= '"school_token": "' . $school->getToken() . '",';
        $json .= '"hours": [';
        
        if(!empty($lessons[0])) {
            $last_hour = (string)$lessons[0]->getHour();
        }
        $i = 1;
        $j = 1;
        $is_first = true;
        $is_first_hour = true;
        foreach($lessons as $lesson) {
            $current_hour = (string)$lesson->getHour();
            if($last_hour != $current_hour || $j == 1) {
                 $i = 1;
                if($j != 1)
                    $json .= ']}';
                if(!$is_first) {
                    $json .= ', ';
                    $is_first = false;
                }
                $json .= '{ "start": "' . $lesson->getHour()->getStart() . '", ';
                $json .= '"end": "' . $lesson->getHour()->getEnd() . '", ';
                $json .= '"hour_id": "' . $lesson->getHour()->getId() . '", ';
                $json .= '"updated_at" : "' . $lesson->getHour()->getUpdatedAt() . '", ';
                $json .= '"lessons": [';
                $is_first_lesson = true;
                $is_first = false;
            }
            if($is_first_lesson)
                $is_first_lesson = false;   
            else
                $json .= ', ';
            $subject = $lesson->getTeacher()->getSubject();
            $json .= '{';
            $json .= '"subject": "' . $lesson->getSubject()->getName() . '", ';
            $json .= '"subject_id": "' . $lesson->getSubject()->getId() . '", ';
            $json .= '"subject_updated_at": "' . $lesson->getSubject()->getUpdatedAt() . '", ';
            $json .= '"room": "' . $lesson->getRoom()->getNumber() . '", ';
            $json .= '"room_id": "' . $lesson->getRoom()->getId() . '", ';
            $json .= '"room_teacher_id": "' . $lesson->getRoom()->getTeacher()->getId() . '", ';
            $json .= '"room_updated_at": "' . $lesson->getRoom()->getUpdatedAt() . '", ';
            $json .= '"teacher": "' . (string)$lesson->getTeacher() . '", ';
            $json .= '"teacher_id": "' . $lesson->getTeacher()->getId() . '", ';
            if($subject != null)
                $json .= '"teacher_subject_id": "' . $subject->getId() . '", ';
            else
                $json .= '"teacher_subject_id": "0", ';
            $json .= '"teacher_name": "' . $lesson->getTeacher()->getName() . '", ';
            $json .= '"teacher_surname": "' . $lesson->getTeacher()->getSurname() . '", ';
            $json .= '"teacher_city": "' . $lesson->getTeacher()->getCity() . '", ';
            $json .= '"teacher_street": "' . $lesson->getTeacher()->getStreet() . '", ';
            $json .= '"teacher_postal": "' . $lesson->getTeacher()->getPostal() . '", ';
            $json .= '"teacher_token": "' . $lesson->getTeacher()->getToken().  '", ';
            $json .= '"teacher_updated_at": "' . $lesson->getTeacher()->getUpdatedAt() . '", ';
            $json .= '"timetable": "' . $lesson->getTimetable()->getName() . '", ';
            $json .= '"timetable_id": "' . $lesson->getTimetable()->getId() . '", ';
            $json .= '"day": "' . $lesson->getDay() . '", ';
            $json .= '"lesson_id": "' . $lesson->getId() . '", ';
            $json .= '"updated_at": "' . $lesson->getUpdatedAt() . '", ';
            $json .= '"replacement": { ';
            if($lesson->getReplacements()->last() != null) {
                $json .= '"is_replacement_null": "false", ';
                $json .= '"replacement_teacher_id": "' . $lesson->getReplacements()->last()->getReplacementTeacherId() . '", ';
                $json .= '"replacement_subject_id": "' . $lesson->getReplacements()->last()->getReplacementSubjectId() . '", ';
                $json .= '"replacement_room_id": "' . $lesson->getReplacements()->last()->getReplacementRoomId() . '", ';
                $json .= '"replacement_hour_id": "' . $lesson->getReplacements()->last()->getReplacementHourId() . '", ';
                $json .= '"description": "' . $lesson->getReplacements()->last()->getDescription() . '", ';
                $json .= '"is_free": "' . $lesson->getReplacements()->last()->getIsEmpty() . '"';
            } else
                $json .= '"is_replacement_null": "true"';
            $json .= ' }';
            $json .= '}';
            $j++;
        }
        $json .= "]}]}}";
        return $json;
    }
    
    private function getAllFromRepository($repositoryName) {
        $repository = $this->getDoctrine()->getRepository($repositoryName);
        return $repository->findAll();
        
    }
    
    private function getSchool() {
        $repository = $this->getDoctrine()->getRepository('mkotlarzQRAppBundle:School');
        return $repository->find(1);
    }
    

}

